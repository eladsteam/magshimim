<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Assistance */

$this->title = 'עריכת סייע: ' . $model->userNumber0->fullName;
$this->params['breadcrumbs'][] = ['label' => 'סייעים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userNumber0->fullName, 'url' => ['view', 'id' => $model->userNumber]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="assistance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
