<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Assistance */

$this->title = 'יצירת סייע';
$this->params['breadcrumbs'][] = ['label' => 'סייעים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assistance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
