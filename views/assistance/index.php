<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AssistanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'סייעים';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/assistance2.jpg" class="img-rounded" height="100" width="300" style="float: left;">
<div class="assistance-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('יצירת סייע', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'userNumber',
            [
			'attribute' => 'user', // New attribute for filter instead of fk userNumber. the functions are at TeacherSearch!
           	'label' => 'שם סייע',
            'format'=>'raw',
                    'value' => function($data){
					 return
                        Html::a($data->userNumber0->fullname, ['assistance/view','id'=>$data->userNumber], ['title' => 'View','class'=>'no-pjax']);
                    }
			],
            'teachingHours',
            'suspendHours',

                          [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
        ],
    ]); ?>
</div>
