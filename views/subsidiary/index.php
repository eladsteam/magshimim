<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubsidiarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'בנות שירות';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/subsidiary2.jpg" class="img-rounded" height="100" width="280" style="float: left;">
<div class="subsidiary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('יצירת בת שירות', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           [ 'attribute' => 'user', // New attribute for filter instead of fk userNumber. the functions are at TeacherSearch!
           	 'label' => 'שם בת שירות',
             'format'=>'raw',
                    'value' => function($data){
					 return
                        Html::a($data->userNumber0->fullname, ['subsidiary/view','id'=>$data->userNumber], ['title' => 'View','class'=>'no-pjax']);
                    }
            ],
            
            //'userNumber',
            'teachingHours',
            'suspendHours',

                         [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
        ],
    ]); ?>
</div>
