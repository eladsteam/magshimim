<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subsidiary */

$this->title = 'יצירת בת שירות';
$this->params['breadcrumbs'][] = ['label' => 'בנות שירות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsidiary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
