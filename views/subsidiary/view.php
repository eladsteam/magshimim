<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Subsidiary */

$this->title = $model->userNumber0->fullName;
$this->params['breadcrumbs'][] = ['label' => 'בנות שירות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsidiary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עריכה', ['update', 'id' => $model->userNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->userNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם הינך בטוח שברצונך למחוק?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'userNumber',
            [
				'attribute' => 'userNumber',
				'label' => 'שם בת השירות',
				'format' => 'raw',
				'value' => function($model){
					return $model->userNumber0->fullname;  //////////Showing subsidadiary name instead of user number.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            'teachingHours',
            'suspendHours',
        ],
    ]) ?>

</div>
