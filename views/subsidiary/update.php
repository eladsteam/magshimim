<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subsidiary */

$this->title = 'עריכת בת שירות: ' . $model->userNumber0->fullName;
$this->params['breadcrumbs'][] = ['label' => 'בנות שירות', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userNumber0->fullName, 'url' => ['view', 'id' => $model->userNumber]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="subsidiary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
