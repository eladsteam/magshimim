<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Subsidiary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subsidiary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userNumber')->dropDownList(User::getTeames2()) ?>

    <?= $form->field($model, 'teachingHours')->textInput() ?>

    <?= $form->field($model, 'suspendHours')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'שמירה', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
