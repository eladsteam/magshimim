<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Specialization */

$this->title = $model->specialization;
$this->params['breadcrumbs'][] = ['label' => 'התמחויות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialization-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עריכה', ['update', 'id' => $model->specId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->specId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם הינך בטוח שברצונך למחוק?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'specId',
            'specialization',
        ],
    ]) ?>

</div>
