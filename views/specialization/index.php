<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpecializationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'התמחות';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/specialization.jpg" class="img-rounded" height="140" width="180" style="float: left;">
<div class="specialization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('יצירת התמחות', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        ['class' => 'yii\grid\SerialColumn'],


            // 'specId',
            'specialization',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
