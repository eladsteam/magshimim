<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Specialization */

$this->title = 'עריכת התמחות: ' . $model->specId;
$this->params['breadcrumbs'][] = ['label' => 'התמחויות', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->specId, 'url' => ['view', 'id' => $model->specId]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="specialization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
