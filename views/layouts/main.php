<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    Yii::$app->user->isGuest ? $brandUrl = ['/activity/index'] :
		$brandUrl = Yii::$app->homeUrl;
	           NavBar::begin([
                'brandLabel' => Html::img('/magshimim/images/magshimim.png', ['alt'=>'Magshimim']),
                // The css for this image is located at bootstrap.css
                'brandUrl' => $brandUrl,
                'options' => [
                    'class' => '',
        ],
    ]);

    
 echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
				
			Yii::$app->user->isGuest ?
			['label' => 'בית', 'url' => ['/site/index']]
			:
			['label' => 'מערכת שעות', 'url' => ['/activity/index']]
			,
            Yii::$app->user->isGuest ? 
            ['label' => 'עלינו', 'url' => ['/site/about']]
			:
			['label' => 'משתמשים',
				'items' => [
                    ['label' => 'אנשי צוות', 'url' => ['/user/index']],
                    ['label' => 'מורים', 'url' => ['/teacher/index'], 'visible' => Yii::$app->user->can('fullCrudPrincipal')], // access control -> this option visible only to admin
                    ['label' => 'בנות שירות', 'url' => ['/subsidiary/index'], 'visible' => Yii::$app->user->can('fullCrudPrincipal')], // access control -> this option visible only to admin
					['label' => 'סייעים', 'url' => ['/assistance/index'], 'visible' => Yii::$app->user->can('fullCrudPrincipal')], // access control -> this option visible only to admin
                ],
            ],
            Yii::$app->user->isGuest ?
			// ['label' => 'יצירת קשר', 'url' => ['/site/contact']]
			// :
            '': // if we want back contact delte  this line
			['label' => 'תלמידים', 'url' => ['/student/index']]
			,
            Yii::$app->user->isGuest ? 
            ''
			:
			['label' => 'ניהול', 'visible' => Yii::$app->user->can('fullCrudPrincipal'), // access control -> this option visible only to admin
				'items' => [
                    ['label' => 'כיתות', 'url' => ['/grade/index']], 
                    ['label' => 'כיתות לימוד', 'url' => ['/classroom/index']], 
                    ['label' => 'התמחויות', 'url' => ['/specialization/index']], 
                ],
            ],
            Yii::$app->user->isGuest ? (
                ['label' => 'התחברות', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'התנתק (' . Yii::$app->user->identity->userName . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    ?>
    <body bgcolor="#349" text="white" onload="startTime()">
            <h5 align="left">
            <span id="txt"></span>
            </h5>
        </body>
    <?php
    NavBar::end();
    ?>

    <div class="container">
         <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 
        ]) ?>
        <script type="text/javascript">
        function startTime(){
            var d=new Date();
            //////// convert day into string
            var weekday = new Array(7);
            weekday[0] = "יום ראשון";
            weekday[1] = "יום שני";
            weekday[2] = "יום שלישי";
            weekday[3] = "יום רביעי";
            weekday[4] = "יום חמישי";
            weekday[5] = "יום שישי";
            weekday[6] = "שבת שלום";
            var n = weekday[d.getDay()];
            ////////// setting the date 
            var h=addZero(d.getHours());
            var m=addZero(d.getMinutes());
            var s=addZero(d.getSeconds());
            var day=d.getDate();
            var month=d.getMonth() +1;
            var year=d.getFullYear();
            // document.getElementById("txt").innerHTML="<b>"+n+"</br>"+h+":"+m+":"+s+ "</br> " +day+ "/" +month+ "/" +year+"</b>"; // clock with bold and 3 lines
            // document.getElementById("txt").innerHTML= n+"</br>"+h+":"+m+":"+s+ "</br> " +day+ "/" +month+ "/" +year; // clock with 3 lines
            document.getElementById("txt").innerHTML= n+ " " +day+ "/" +month+ "/" +year + " " +h+":"+m+":"+s; // clear clock
            setTimeout('startTime()', 1000);
        }
        /// funtion for adding zero into the date
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        </script>
       
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; אלעד כהן וניסים סמרלי <?= date('Y') ?></p>

      
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
