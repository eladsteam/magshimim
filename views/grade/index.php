<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GradeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'כיתות';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/grade.jpg" class="img-rounded" height="140" width="180" style="float: left;">
<div class="grade-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('יצירת כיתה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'gradeId',
            'gradeName',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
