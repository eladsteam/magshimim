<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Grade */

$this->title = 'עריכת כיתה: ' . $model->gradeName;    
$this->params['breadcrumbs'][] = ['label' => 'כיתות', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gradeName, 'url' => ['view', 'id' => $model->gradeId]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="grade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
