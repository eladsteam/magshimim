<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'שחזור סיסמה למערכת';
?>
<div class="wrap">

    <h3><?= $msg ?></h3>
    
    <h1>שחזור סיסמה</h1>
    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'enableClientValidation' => true,
    ]);
    ?>
    
    <div class="form-group">
    <?= $form->field($model, "email")->input("email")->label('הזן כתובת דואר אלקטרוני') ?>  
    </div>
    
    <?= Html::submitButton("שלח לי מייל", ["class" => "btn btn-primary"]) ?>
    
    <?php $form->end() ?>

</div>