<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'איפוס סיסמה למערכת';
?>

<div class="wrap">
    <h3><?= $msg ?></h3>
    
    <h1>איפוס סיסמה</h1>
    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'enableClientValidation' => true,
    ]);
    ?>

    <div class="form-group">
    <?= $form->field($model, "email")->input("email", ['placeholder' => "הזן את כתובת הדואר האלקטרוני שלך"]) ?>  
    </div>
    
    <div class="form-group">
    <?= $form->field($model, "password")->input("password", ['placeholder' => "בחר סיסמה חדשה"]) ?>  
    </div>
    
    <div class="form-group">
    <?= $form->field($model, "password_repeat")->input("password", ['placeholder' => "הזן את הסיסמה בשנית"]) ?>  
    </div>

    <div class="form-group">
    <?= $form->field($model, "verification_code")->input("text", ['placeholder' => "קוד האימות שנשלח אליך בדואר האלקטרוני"]) ?>  
    </div>
    
    <?= Html::submitButton("אפס סיסמה", ["class" => "btn btn-primary"]) ?>
    
    <?php $form->end() ?>
</div>