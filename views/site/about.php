<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'עלינו';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
p {
    display: block;
    margin-top: 1em;
    margin-bottom: 1em;
    margin-left: 0;
    margin-right: 0;
    text-align: center;
    font-size: 120%;
    font: arial, sans-serif;
}

</style>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

<p>  <b>      
    מערכת מידע זו פותחה במסגרת פרויקט הגמר של המחלקה להנדסת תעשיה וניהול במכללת עזריאלי.</br> 
    המערכת פותחה בהתאם לדרישות ולצרכים של בית הספר לחינוך מיוחד "מגשימים", ומטרתה לסייע בשיבוץ תלמידים ומורים במערכת השעות.</br> כמו כן, המערכת מסייעת במעקב אחר הפעילות השוטפת המתנהלת בבית הספר.
    </br>לתמיכה בעת הצורך ניתן לשלוח מייל לכתובות:</br>
</b> </p>
<p>
    <a href="mailto:alad17@gmail.com?Subject=Hello" target="_top">alad17@gmail.com</a>
    </br>
    </br>
    <a href="mailto:nisimsamareli@gmail.com?Subject=Hello" target="_top">nisimsamareli@gmail.com</a>
</p>

    
<img id="contactform-verifycode-image" src="/magshimim/images/team2.jpg" alt="" height="200" width="300" style="float: left">
    
</div>
