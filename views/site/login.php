<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'התחברות';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>אנא מלא את השדות הבאים על מנת להתחבר:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'control-label'],
        ],
    ]); ?>
                <!-- We defined the label names in LoginForm.php  -->
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>


                          <!--     Show password    -->
		<?= $form->field($model, 'password')->passwordInput() ?> &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		<div class="checkbox-1">
            <?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('הצג סיסמה', 'reveal-password') ?>
            <br>
            <?php 
            $this->registerJs("jQuery('#reveal-password').change(function(){jQuery('#loginform-password').attr('type',this.checked?'text':'password');})");
            ?>
        </div>
				<!--  End of show password  -->
        <div class="checkbox-1">
            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"\">{input} {label}</div>\n<div class=\"\">{error}</div>",
                'label'=>"<b>זכור אותי</b>",
            ]) ?>
        </div>

        <div class="forgot_pwd" style="cursor:pointer">
        <b> <a href="http://localhost/magshimim/web/site/recoverpass">שכחת סיסמה ?</a> </b>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9" style="width=20px;">
                <?= Html::submitButton('כניסה', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

        

    <?php ActiveForm::end(); ?>

</div>

