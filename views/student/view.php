<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = $model->nickName;
$this->params['breadcrumbs'][] = ['label' => 'תלמידים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php //access control -> hide button ?>
    <?php if (\Yii::$app->user->can('fullCrudPrincipal')) { ?>
    <p>
        <?= Html::a('עריכה', ['update', 'id' => $model->studentId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->studentId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם הינך בטוח שברצונך למחוק?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'studentId',
            'studentIdOrigin',
            'firstName',
            'lastName',
            'nickName',
            [ // line for age, the getBdayCalculation function declared in student.php
                'attribute'=>'גיל',
				'value'=> Html::encode($model->getBdayCalculation($model->birth)),	
			],
            'address',
            'phone',
            'birth',
            // 'grade',
            [ // the grade name 
				'label' => $model->attributeLabels()['grade'],
				'value' => $model->grade0->gradeName,	
			],
            'hmo',
            'mother_phone',
            'mother_address',
            'father_phone',
            'father_address',
            'hostel',
            'hostel_contect',
            'hostel_mail',
        ],
    ]) ?>

</div>
