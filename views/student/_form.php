<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Grade;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'studentIdOrigin')->textInput() ?>

    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nickName')->textInput(['placeholder' => "שם התלמיד אשר יופיע במערכת"], ['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput() ?>

    <?= $form->field($model, 'birth')->widget(DatePicker::className(), [
		//'size' => 'lg',
		'language'=>'he',
        'options' => ['placeholder' => 'בחר/י תאריך לידה'],
		'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            //'todayHighlight' => true // Default today			
        ]
	]);?>

    <?= $form->field($model, 'grade')-> dropDownList(Grade::getStudentGrade()) ?>

    <?= $form->field($model, 'hmo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mother_phone')->textInput() ?>

    <?= $form->field($model, 'mother_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'father_phone')->textInput() ?>

    <?= $form->field($model, 'father_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hostel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hostel_contect')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hostel_mail')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'שמירה', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
