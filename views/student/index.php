<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'תלמידים';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/students.jpg" class="img-rounded" height="100" width="180" style="float: left;">
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php //access control -> hide button ?>
    <?php if (\Yii::$app->user->can('fullCrudPrincipal')) { ?>
        <p>
            <?= Html::a('יצירת תלמיד', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'studentId',
            // 'studentIdOrigin',
            // 'firstName',
            [   
                    'attribute'=>'firstName',
                    'format'=>'raw',
                    'value' => function($data)
                    {
                        return
                        Html::a($data->firstName, ['student/view','id'=>$data->studentId], ['title' => 'View','class'=>'no-pjax']);
                    }
            ],
            'lastName',
            'nickName',
            // 'address',
            // 'phone',
            // 'birth',
            // 'grade',
            [
				'attribute' => 'grade',
				//'label' => 'Student Grade',
				'format' => 'raw',
				'value' => function($model){
					return $model->grade0->gradeName;  //////////Showing role name instead of role id.
				},
				'filter'=>Html::dropDownList('StudentSearch[grade]', $studentGrade, $studentGrades, ['class'=>'form-control']),   //////////////// the arguments are from the controller!				
			],
            // 'hmo',
            // 'mother_phone',
            // 'mother_address',
            // 'father_phone',
            // 'father_address',
            // 'hostel',
            // 'hostel_contect',
            // 'hostel_mail',

            [        
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
        ],
    ]); ?>
</div>
