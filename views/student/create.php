<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = 'יצירת תלמיד';
$this->params['breadcrumbs'][] = ['label' => 'תלמידים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="יצירת תלמיד">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
