<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = 'עריכת תלמיד: ' . $model->studentId;
$this->params['breadcrumbs'][] = ['label' => 'תלמידים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->studentId, 'url' => ['view', 'id' => $model->studentId]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="student-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
