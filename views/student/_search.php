<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'studentId') ?>

    <?= $form->field($model, 'studentIdOrigin') ?>

    <?= $form->field($model, 'firstName') ?>

    <?= $form->field($model, 'lastName') ?>

    <?= $form->field($model, 'nickName') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'birth') ?>

    <?php // echo $form->field($model, 'grade') ?>

    <?php // echo $form->field($model, 'hmo') ?>

    <?php // echo $form->field($model, 'mother_phone') ?>

    <?php // echo $form->field($model, 'mother_address') ?>

    <?php // echo $form->field($model, 'father_phone') ?>

    <?php // echo $form->field($model, 'father_address') ?>

    <?php // echo $form->field($model, 'hostel') ?>

    <?php // echo $form->field($model, 'hostel_contect') ?>

    <?php // echo $form->field($model, 'hostel_mail') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
