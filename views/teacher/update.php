<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Teacher */

$this->title = 'עריכת מורה: ' . $model->userNumber0->fullName;
$this->params['breadcrumbs'][] = ['label' => 'מורים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userNumber0->fullName, 'url' => ['view', 'id' => $model->userNumber]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="teacher-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
