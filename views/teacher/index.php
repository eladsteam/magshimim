<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'מורים';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/teachers2.jpg" class="img-rounded" height="140" width="180" style="float: left;">
<div class="teacher-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('יצירת מורה', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'userNumber',
            // [
			// 	'attribute' => 'userNumber',
			// 	//'label' => 'Teacher Name',
			// 	'format' => 'raw',
			// 	'value' => function($model){
			// 		return $model->userNumber0->fullname;  //////////Showing teacher name instead of user number.
			// 	},
			// 	//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			// ],
             [
              'attribute' => 'user', // New attribute for filter instead of fk userNumber. the functions are at TeacherSearch!
              'label' => 'שם מורה',
              'format'=>'raw',
                    'value' => function($data){
					 return
                        Html::a($data->userNumber0->fullname, ['teacher/view','id'=>$data->userNumber], ['title' => 'View','class'=>'no-pjax']);
                    }
            ],
            //'specializationName',
            [
				'attribute' => 'specializationName',
				//'label' => 'Specialization Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->specializationName0->specialization;  //////////Showing teacher name instead of teacher number.
				},
				'filter'=>Html::dropDownList('TeacherSearch[specializationName]', $teacherSpecialization, $teacherSpecializations, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            'teachingHours',
            'suspendHours',

                         [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
        ],
    ]); ?>
</div>
