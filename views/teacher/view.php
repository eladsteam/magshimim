<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Teacher */

$this->title = $model->userNumber0->fullName;
$this->params['breadcrumbs'][] = ['label' => 'מורים', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עריכה', ['update', 'id' => $model->userNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->userNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם הינך בטוח שברצונך למחוק?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'userNumber',
            [
				'attribute' => 'userNumber',
				'label' => 'שם המורה',
				'format' => 'raw',
				'value' => function($model){
					return $model->userNumber0->fullname;  //////////Showing teacher name instead of teacher number.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            //'specializationName',
            [
				'attribute' => 'specializationName',
				//'label' => 'Specialization Name',
				'format' => 'raw',
				'value' => function($model){
					return $model->specializationName0->specialization;  //////////Showing teacher name instead of user number.
				},
				//'filter'=>Html::dropDownList('CourseClassSearch[teacherId]', $teacher, $teachers, ['class'=>'form-control']),   //////////////// the arguments are from the controller!
			],
            'teachingHours',
            'suspendHours',
        ],
    ]) ?>

</div>
