<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Specialization;
use yii\helpers\ArrayHelper
/* @var $this yii\web\View */
/* @var $model app\models\Teacher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-form">

    <?php $form = ActiveForm::begin(); ?>

   <?= $form->field($model, 'userNumber')->dropDownList(User::getTeames1()) ?>

    <?= $form->field($model, 'specializationName')->dropDownList(specialization::getSpecializationName())?>

    <?= $form->field($model, 'teachingHours')->textInput() ?>

    <?= $form->field($model, 'suspendHours')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'יצירה' : 'שמירה', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
