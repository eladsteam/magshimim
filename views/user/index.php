<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'אנשי צוות';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/user.jpg" class="img-rounded" height="140" width="180" style="float: left;">
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php //access control -> hide button ?>
<?php if (\Yii::$app->user->can('fullCrudPrincipal')) { ?>
    <p>
        <?= Html::a('יצירת איש צוות', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [   
                    'attribute'=>'userName',
                    'format'=>'raw',
                    'value' => function($data)
                    {
                        return
                        Html::a($data->userName, ['user/view','id'=>$data->userNumber], ['title' => 'View','class'=>'no-pjax']);
                    }
            ],

            //'userNumber',
            //'userId',
          
            //'password',
            //'auth_key',
             'firstName',
             'lastName',
             'nickName',
            // 'birth',
            // 'email:email',
             'phone',
            // 'address',
            // 'roleName',
            // 'startWork',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
			 [
				'attribute' => 'roleName',
				//'label' => 'user Role',
				'format' => 'raw',
				'value' => function($model){
					return $model->roleName0->roleType;  //////////Showing role name instead of role id.
				},
				'filter'=>Html::dropDownList('UserSearch[roleName]', $userRole, $userRoles, ['class'=>'form-control']),   //////////////// the arguments are from the controller!				
			],
                  [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
            // ['class' => 'yii\grid\ActionColumn'], show all the options (delete, view, edit)
        ],
    ]); ?>
</div>
