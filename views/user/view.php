<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->fullName;
$this->params['breadcrumbs'][] = ['label' => 'אנשי צוות', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עריכה', ['update', 'id' => $model->userNumber], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->userNumber], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם הינך בטוח שברצונך למחוק?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'userNumber',
            'userId',
            'userName',
            //'password',
            //'auth_key',
            'firstName',
            'lastName',
            'nickName',
            'birth',
            'email:email',
            'phone',
            'address',
            //'roleName',
			[ // the role name 
				'label' => $model->attributeLabels()['roleName'],
				'value' => $model->roleName0->roleType,	
			],
            [ // The permission of the user
				'label' => $model->attributeLabels()['role'],
				'value' => $model->userole,
			],
            'startWork',
            //'created_by',
            ['attribute'=>'createUserName', 'format'=>'raw'],
            'created_at:datetime',
            //'updated_by',
            ['attribute'=>'updateUserName', 'format'=>'raw'],
            'updated_at:datetime',
            [ // teaching Hours 
				'label' => "שעות הוראה",
				'value' => $model->teachers2->teachingHours,	
			],
            [ // suspend Hours 
				'label' => "שעות שהייה",
				'value' => $model->teachers2->suspendHours,	
			],
        ],
    ]) ?>

</div>

<?php
/////////variable of year////////
 $currentYear = date("Y");
        $value0 = "2017";
        $value1 = "2018";
        $value2 = "2019";
        $value3 = "2020";
        if (isset($_POST["FruitList"])) {
            $submittedValue = $_POST["FruitList"];
        }
        else{
            $submittedValue = $currentYear; // if the user didn't choose any year he will see the current year
        }
?>
        <!--    dropdownlist of years    -->
    <form action="" name="fruits" method="post"> 
    <br>
    <h3> שעות אנשי צוות בכל שנה</h3> 
        <select project="FruitList" id="FruitList" name="FruitList" class="btn btn-md" value='$_POST["FruitList"]' style="font-weight:bold;">
            <option value = "<?php echo $value0; ?>"<?php echo ($value0 == $submittedValue)?" SELECTED":""?>><?php echo $value0; ?></option>
            <option value = "<?php echo $value1; ?>"<?php echo ($value1 == $submittedValue)?" SELECTED":""?>><?php echo $value1; ?></option>
            <option value = "<?php echo $value2; ?>"<?php echo ($value2 == $submittedValue)?" SELECTED":""?>><?php echo $value2; ?></option>
            <option value = "<?php echo $value3; ?>"<?php echo ($value3 == $submittedValue)?" SELECTED":""?>><?php echo $value3; ?></option>
        </select>
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" value=' $submittedValue' />
        <!--   the row above is necessary for yii  -->
        <input type="submit" name="submit" id="submit" value="הצג" class="btn btn-primary" />
    </form>

<div style="display: none">
	<?php
		echo Highcharts::widget([
		'scripts' => [
			'highcharts-more',
			//'themes/grid',
			'highcharts-3d',
			'modules/drilldown'
		]	
	]);
	?>	
</div>

<div id="chart"></div>

<?php // query for basic chart according to filtring by year $submittedValue-%
$sql = "SELECT DATE_FORMAT(start, '%Y-%M') as start, SUM(timeDifferent/60) as hours, UA.userNumber 
        FROM useractivity UA, activity A 
        WHERE A.activityId=UA.activityId 
        AND start LIKE '$submittedValue-%' 
        AND UA.userNumber='$model->id' 
        AND `activityTypeId`!='4' 
        GROUP BY DATE_FORMAT(start, '%m')";
        // every detail the added to this query need to be added to the second query!!!
        // we don't want to include the vacation hours and staining hours therefore we removed the activityTypeId
$rawData = yii::$app->db->createCommand($sql)->queryAll();
$main_data =[];
foreach ($rawData as $data){
	$main_data[] =[
	'name'=>$data['start'],
	'y' => $data['hours'] *1,
	'drilldown' => $data ['hours']
	];	
}

// query for drill chart according to filtring by year $submittedValue-% 
$main = json_encode($main_data);

    $sql = "select month(g.start) as month 
    ,SUM(g.timeDifferent/60) as total

    ,SUM(g1.timeDifferent/60) as first
    ,SUM(g2.timeDifferent/60)  as sec

    from activity g 
    join useractivity u on u.activityId = g.activityId
    left join activity g1 on g.start = g1.start and g.activityTypeId ='1' 
    left join activity g2 on g.start = g2.start and g.activityTypeId ='2' 

    where u.userNumber='$model->id' AND g.start LIKE '$submittedValue-%' AND g.activityTypeId !='4'

    group by month(g.start)";


$rawData = yii::$app->db->createCommand($sql)->queryAll();
$sub_data =[];
foreach ($rawData as $data){
	$sub_data[] =[
	'id' => $data['total'],
	'name' => $data['month'] *1,
	'data' => [['שיעור',$data['first']*1], ['שהייה',$data['sec']*1]
	]];	
}

$sub = json_encode($sub_data);	
	
	 $this->registerJs("$(function () {
    // Create the chart
    $('#chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'דוח שעות עבור איש צוות'
        },
        subtitle: {
        text: 'לחץ על עמודה על מנת לראות את חלוקת השעות בחודש' 
        },
        credits: false,
        xAxis: {
            type: 'category',
			        useHTML: Highcharts.hasBidiBug,
					useHTML:true	
        },
        yAxis: {
            title: {
                text: ' כמות שעות',
				  useHTML: Highcharts.hasBidiBug,
				  useHTML:true
				  },
        },
        
		labels: {
				useHTML: Highcharts.hasBidiBug,
				useHTML:true
				},
        legend: {
            enabled: true,
			useHTML:true
        },
		tooltip: {
        useHTML: true
        },
        plotOptions: {
            series: {	
                borderWidth: 0,
				useHTML:true,
                dataLabels: {
                    enabled: true,
					useHTML: Highcharts.hasBidiBug,
					useHTML:true,
                    format: '{point.y}'
                }
            }
			
        },
        series: [{
            name: 'חודשים',
            colorByPoint: true,
		 useHTML: Highcharts.hasBidiBug,	
            data: $main,
			useHTML:true	
        }],
          drilldown: {
            series: $sub,
			useHTML:true,
		}
    });
});");


?>