<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Role;
use app\models\Specialization;
//use app\models\Teacher;
//use dosamigos\datepicker\DateRangePicker;
//use dosamigos\datepicker\DatePicker;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'userName')->textInput(['placeholder' => "שם המשתמש לצורך כניסה למערכת"], ['maxlength' => true]) ?>

	<?= $form->field ($model, 'password')->passwordInput(['placeholder' => "בחר/י סיסמה בת 4 תווים לפחות"], ['maxlength' => true]) // maybe to change that on update we can't see the password?>

    <?/// auth_key field is cancelled?> 

    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nickName')->textInput(['placeholder' => "שם המשתמש אשר יופיע במערכת"], ['maxlength' => true]) ?>
	
    <?= $form->field($model, 'birth')->widget(DatePicker::className(), [
		//'size' => 'lg',
		'language'=>'he',
        'options' => ['placeholder' => 'בחר/י תאריך לידה'],
		'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            //'todayHighlight' => true // Default today			
        ]
	]);?>

    <?= $form->field($model, 'email')->textInput(['placeholder' => "מומלץ להזין כתובת דואר אלקטרוני למטרת שחזור סיסמה"], ['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'startWork')->widget(DatePicker::className(), [
		//'size' => 'lg',
		'language'=>'he',
        'options' => ['placeholder' => 'בחר/י תאריך תחילת עבודה'],
		'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true // Default today			
        ]
	]);?>
	
    <?= $form->field($model, 'roleName')->dropDownList(Role::getUserRole()) ?>
    
    <?= $form->field($model, 'role')->dropDownList($roles, ['options' => [ 'צפייה בלבד' => ['selected ' => true]]]) ?>	

    <!--Create Teacher hours for this user -->
    

    


    <?/// created_at field is cancelled?> 
	
	<?/// updated_at field is cancelled?> 
	
	<?/// created_by field is cancelled?> 
	
	<?/// updated_by field is cancelled?> 
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'הבא' : 'שמירה', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
