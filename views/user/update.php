<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'עריכת משתמש: ' . $model->userNumber;
$this->params['breadcrumbs'][] = ['label' => 'משתמשים', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userNumber, 'url' => ['view', 'id' => $model->userNumber]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        //'teacher' => $teacher,
        'roles' => $roles, // for permission level 
    ]) ?>

</div>
