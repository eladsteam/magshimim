<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Classroom */

$this->title = $model->classroomName;
$this->params['breadcrumbs'][] = ['label' => 'כיתות לימוד', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classroom-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('עריכה', ['update', 'id' => $model->classroomId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('מחיקה', ['delete', 'id' => $model->classroomId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'האם הינך בטוח שברצונך למחוק?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'classroomId',
            'classroomName',
        ],
    ]) ?>

</div>
