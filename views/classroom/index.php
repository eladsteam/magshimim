<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClassroomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'כיתות לימוד';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/magshimim/images/classroom.jpg" class="img-rounded" height="140" width="180" style="float: left;">
<div class="classroom-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('יצירת כיתת לימוד', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'classroomId',
            'classroomName',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
