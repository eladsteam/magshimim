<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Classroom */

$this->title = 'עריכת כיתת לימוד: ' . $model->classroomName;
$this->params['breadcrumbs'][] = ['label' => 'כיתות לימוד', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->classroomId, 'url' => ['view', 'id' => $model->classroomId]];
$this->params['breadcrumbs'][] = 'עריכה';
?>
<div class="classroom-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
