<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Classroom */

$this->title = 'יצירת כיתת לימוד';
$this->params['breadcrumbs'][] = ['label' => 'כיתות לימוד', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classroom-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
