<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\widgets\DetailView;
use app\models\Grade;
use \DateTime;

/**
 * This is the model class for table "student".
 *
 * @property integer $studentId
 * @property integer $studentIdOrigin
 * @property string $firstName
 * @property string $lastName
 * @property string $nickName
 * @property string $address
 * @property integer $phone
 * @property string $birth
 * @property integer $grade
 * @property string $hmo
 * @property integer $mother_phone
 * @property string $mother_address
 * @property integer $father_phone
 * @property string $father_address
 * @property string $hostel
 * @property string $hostel_contect
 * @property string $hostel_mail
 *
 * @property Grade $grade0
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['studentIdOrigin', 'phone', 'grade', 'mother_phone', 'father_phone'], 'integer'],
            [['studentIdOrigin', 'nickName'], 'required'],  
            [['birth'], 'safe'],
            [['firstName', 'lastName', 'nickName'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9 -]+$/', 'message' => 'אין אפשרות להוסיף תווים מיוחדים בשדה זה, נא הזן אותיות ומספרים בלבד'],
            [['firstName', 'lastName', 'nickName', 'address', 'hmo', 'mother_address', 'father_address', 'hostel', 'hostel_contect', 'hostel_mail'], 'string', 'max' => 255],
            [['grade'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['grade' => 'gradeId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'studentId' => 'מספר סטודנט',
            'studentIdOrigin' => 'תעודת זהות',
            'firstName' => 'שם פרטי',
            'lastName' => 'שם משפחה',
            'nickName' => 'כינוי',
            'address' => 'כתובת',
            'phone' => 'טלפון',
            'birth' => 'תאריך לידה',
            'grade' => 'כיתה',
            'hmo' => 'קופת חולים',
            'mother_phone' => 'טלפון אם',
            'mother_address' => 'כתובת אם',
            'father_phone' => 'טלפון אב',
            'father_address' => 'כתובת אב',
            'hostel' => 'הוסטל',
            'hostel_contect' => 'איש קשר בהוסטל',
            'hostel_mail' => 'דואר אלקטרוני בהוסטל',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade0()
    {
        return $this->hasOne(Grade::className(), ['gradeId' => 'grade']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentactivities()
    {
        return $this->hasMany(Studentactivity::className(), ['studentId' => 'studentId']);
    }

    // This function is for gatting the student age and called in view/student.php
    function getBdayCalculation($bday)
    { 
    $datetime1 = new DateTime($bday);
    $datetime2 = new DateTime();
    $diff = $datetime1->diff($datetime2);
    return $diff->y;
    }

}
