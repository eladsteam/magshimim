<?php

namespace app\models;

use Yii;
use app\models\Specialization;

/**
 * This is the model class for table "teacher".
 *
 * @property integer $userNumber
 * @property integer $specializationName
 * @property integer $teachingHours
 * @property integer $suspendHours
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['specializationName', 'teachingHours', 'suspendHours'], 'integer'],
            [['specializationName'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::className(), 'targetAttribute' => ['specializationName' => 'specId']],
            [['userNumber'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userNumber' => 'userNumber']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userNumber' => 'שם המורה',
            'specializationName' => 'שם ההתמחות',
            'teachingHours' => 'שעות הוראה',
            'suspendHours' => 'שעות שהייה',
        ];
    }

    public function getUserNumber0()
    {
        return $this->hasOne(User::className(), ['userNumber' => 'userNumber']);
    }

    public function getSpecializationName0()
    {
        return $this->hasOne(Specialization::className(), ['specId' => 'specializationName']);
    }
}
