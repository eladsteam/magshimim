<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\widgets\DetailView;
use app\models\Role;
use app\models\Teacher;
use app\models\Subsidiary; 
use app\models\Assistance; 
use app\models\Specialization;



/**
 * This is the model class for table "user".
 *
 * @property integer $userNumber
 * @property integer $userId
 * @property string $userName
 * @property string $password
 * @property string $auth_key
 * @property string $firstName
 * @property string $lastName
 * @property string $nickName
 * @property string $birth
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property integer $roleName
 * @property string $startWork
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Assistance[] $assistances
 * @property Subsidiary[] $subsidiaries
 * @property Teacher[] $teachers
 * @property Role $roleName0
 * @property Useractivity[] $useractivities
 * @property Vecation[] $vecations
 * @property string $verification_code

 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    public $role; // for auth_assignment table
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'roleName', 'created_at', 'updated_at', 'created_by', 'updated_by','phone'], 'integer'],
            [['userId', 'userName','firstName', 'lastName', 'nickName','password'], 'required'],
            [['firstName','lastName','nickName','address'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9 -]+$/', 'message' => 'אין אפשרות להוסיף תווים מיוחדים בשדה זה, נא הזן אותיות ומספרים בלבד'],
            [['userName'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9_-]+$/', 'message' => 'אין אפשרות להוסיף תווים מיוחדים בשדה זה, נא הזן שם משתמש רציף ללא רווחים'],
            [['birth', 'startWork', 'role'], 'safe'], // role is for auth_assignment table
            [['userName', 'password', 'auth_key', 'firstName', 'lastName', 'nickName', 'email', 'phone', 'address'], 'string', 'max' => 255],
            [['roleName'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['roleName' => 'roleId']],
            ['password', 'match', 'pattern' => "/^.{4,16}$/", 'message' => 'הסיסמה חייבת להיות לפחות 4 תווים ומקסימום 16 תווים'],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userNumber' => 'מספר משתמש',
            'userId' => 'תעודת זהות',
            'userName' => 'שם משתמש',
            'password' => 'סיסמה',
            'auth_key' => 'Auth Key',
            'firstName' => 'שם פרטי',
            'lastName' => 'שם משפחה',
            'nickName' => 'כינוי',
            'birth' => 'תאריך לידה',
            'email' => 'דואר אלקטרוני',
            'phone' => 'טלפון',
            'address' => 'כתובת',
            'roleName' => 'תפקיד',
            'role' => 'הרשאה',
            'startWork' => 'תחילת עבודה',
            'verification_code'=>'קוד אימות',
            'created_at' => 'נוצר בתאריך',
            'updated_at' => 'עודכן בתאריך',
            'created_by' => 'נוצר ע"י',
            'updated_by' => 'נערך ע"י',
            'createUserName' => Yii::t('app', 'נוצר ע"י'),
            'updateUserName' => Yii::t('app', 'נערך ע"י'),    
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssistances()
    {
        return $this->hasMany(Assistance::className(), ['userNumber' => 'userNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubsidiaries()
    {
        return $this->hasMany(Subsidiary::className(), ['userNumber' => 'userNumber']);
    }

    public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],

				],
			],
		];
    }

	
	public static function findIdentity($id) 
    {
        return static::findOne($id);
    }

    ////////////// functions for blamable behaviors
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['userNumber' => 'created_by']);
    }

    public function getCreateUserName() 
    {
        return $this->createUser ? $this->createUser->fullName : 'לא קיים'; // The fullName will showen in the "created by" field (view.php)
    }

    public function getUpdateUser()
    {
    return $this->hasOne(User::className(), ['userNumber' => 'updated_by']);
    }

    public function getUpdateUserName() 
    {
        return $this->createUser ? $this->updateUser->fullName : 'לא קיים'; // The fullName will showen in the "update by" field (view.php)
    }
    /////////////
	
	public static function findIdentityByAccessToken($token, $type = null) 
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }
	
	 public function getId() ////////////// This function important for recognize the user that log in . login by userNumber or userId
    {
        return $this->userNumber; ////////////////////// userId before
    }
	
		public function getFullname() 
    {
        return $this->firstName.' '.$this->lastName;
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::className(), ['userNumber' => 'userNumber']);
    }
	
	public static function findByUsername($userName)
	{
		return static::findOne(['userName' => $userName]);
	}
    
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}

	public function getUserole() // function for display the permission level at user/view
    {
		$roleArray = Yii::$app->authManager->
					getRolesByUser($this->id);
		$role = array_keys($roleArray)[0];				
		return	$role;
    }
    
    public static function getRoles() // for auth_assignment table
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name;
		}
        
		return $roles; 	
	}
	
	
	public function afterSave($insert,$changedAttributes) 
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->role;   // The role that the user choose  
		$role = $auth->getRole($roleName); // The roles from auth_item table. came from the controller
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} 
        // else {
		// 	$db = \Yii::$app->db;
		// 	$db->createCommand()->delete('auth_assignment',
		// 		['user_id' => $this->id])->execute();
		// 	$auth->assign($role, $this->id);
		// }

        return $return;	
	}
	
	
	public function validatePassword($password) 
	{
		return $this->isCorrectHash($password, $this->password); 
	}
	
	    public function getAuthKey() 
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) 
    {
         return $this->getAuthKey() === $authKey;
    }	
	
	public function beforeSave($insert) 
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleName0()
    {
        return $this->hasOne(Role::className(), ['roleId' => 'roleName']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseractivities()
    {
        return $this->hasMany(Useractivity::className(), ['userNumber' => 'userNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVecations()
    {
        return $this->hasMany(Vecation::className(), ['userNumber' => 'userNumber']);
    }

    public static function getTeames1()  
	{
		$allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('user')
           ->where(['roleName' => '1'])
           ->orderBy(['userNumber' => SORT_DESC])
           ->limit(10)
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'userNumber', 'userName');
		return $allTeamesArray;						
	}

    public static function getTeames2()  
	{
		$allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('user')
           ->where(['roleName' => '2'])
           ->orderBy(['userNumber' => SORT_DESC])
           ->limit(10)
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'userNumber', 'userName');
		return $allTeamesArray;						
	}

    public static function getTeames3()  
	{
		$allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('user')
           ->where(['roleName' => '3'])
           ->orderBy(['userNumber' => SORT_DESC])
           ->limit(10)
           ->all();
		$allTeamesArray = ArrayHelper::
					map($allTeames, 'userNumber', 'userName');
		return $allTeamesArray;						
	}

    public static function createRandomCode() { // verification code while creating new user

    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
    srand((double)microtime()*1000000); 
    $i = 0; 
    $pass = '' ; 

        while ($i <= 7) { 
            $num = rand() % 33; 
            $tmp = substr($chars, $num, 1); 
            $pass = $pass . $tmp; 
            $i++; 
        } 
    return $pass; 
    }

    public function getTeachers2()
    {
      $id = $_GET['id'];
      if(Teacher::find()->where(['userNumber'=>$id])->exists()){
          return $this->hasOne(Teacher::className(), ['userNumber' => 'userNumber']);
      }
      elseif(Subsidiary::find()->where(['userNumber'=>$id])->exists()){
          return $this->hasOne(Subsidiary::className(), ['userNumber' => 'userNumber']);
      }
      elseif(Assistance::find()->where(['userNumber'=>$id])->exists()){
          return $this->hasOne(Assistance::className(), ['userNumber' => 'userNumber']);
      }
      else{
          return NULL;
      }
    }

    public static function getTeames12($id)  
	{
        $allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('user')
           ->where(['userNumber' => $id])
           ->limit(10)
           ->exists(); 
           if($allTeames){
            return $allTeames;
           }
           else{
               return NULL;
           }							
	}

}