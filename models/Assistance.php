<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assistance".
 *
 * @property integer $userNumber
 * @property integer $teachingHours
 * @property integer $suspendHours
 *
 * @property User $userNumber0
 */
class Assistance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assistance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teachingHours', 'suspendHours'], 'integer'],
            // [['suspendHours'], 'required'],
            [['userNumber'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userNumber' => 'userNumber']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userNumber' => 'שם הסייע',
            'teachingHours' => 'שעות הוראה',
            'suspendHours' => 'שעות שהייה',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNumber0()
    {
        return $this->hasOne(User::className(), ['userNumber' => 'userNumber']);
    }
}
