<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Student;

/**
 * StudentSearch represents the model behind the search form about `app\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['studentId', 'studentIdOrigin', 'phone', 'grade', 'mother_phone', 'father_phone'], 'integer'],
            [['firstName', 'lastName', 'nickName', 'address', 'birth', 'hmo', 'mother_address', 'father_address', 'hostel', 'hostel_contect', 'hostel_mail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->grade == -1 ? $this->grade = null : $this->grade;  //necessary for dropdown

        // grid filtering conditions
        $query->andFilterWhere([
            'studentId' => $this->studentId,
            'studentIdOrigin' => $this->studentIdOrigin,
            'phone' => $this->phone,
            'birth' => $this->birth,
            'grade' => $this->grade,
            'mother_phone' => $this->mother_phone,
            'father_phone' => $this->father_phone,
        ]);

        $query->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'lastName', $this->lastName])
            ->andFilterWhere(['like', 'nickName', $this->nickName])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'hmo', $this->hmo])
            ->andFilterWhere(['like', 'mother_address', $this->mother_address])
            ->andFilterWhere(['like', 'father_address', $this->father_address])
            ->andFilterWhere(['like', 'hostel', $this->hostel])
            ->andFilterWhere(['like', 'hostel_contect', $this->hostel_contect])
            ->andFilterWhere(['like', 'hostel_mail', $this->hostel_mail]);

        return $dataProvider;
    }
}
