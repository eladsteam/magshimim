<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "classroom".
 *
 * @property integer $classroomId
 * @property string $classroomName
 *
 * @property Activity[] $activities
 */
class Classroom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'classroom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['classroomName'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9 -]+$/', 'message' => 'אין אפשרות להוסיף תווים מיוחדים בשדה זה, נא הזן אותיות ומספרים בלבד'],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'classroomId' => 'מספר כיתה',
            'classroomName' => 'שם הכיתה',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['classroomId' => 'classroomId']);
    }

    ////////////////
		public static function getClassroom() ///////////////////// new function for dropdown
	{
		$allUserRole = self::find()->all();
		$allUserRoleArray = ArrayHelper::
					map($allUserRole, 'classroomId', 'classroomName');
		return $allUserRoleArray;						
	}

    public static function getClassroomWithAllClassrooms()   //////////////// new function - necessary for dropdown 
	{
		$allUserRole = self::getClassroom();
		$allUserRole[-1] = 'כל הכיתות';
		$allUserRole = array_reverse ( $allUserRole, true );
		return $allUserRole;	
	}
	////////////////////////////
}
