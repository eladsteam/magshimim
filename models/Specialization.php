<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "specialization".
 *
 * @property integer $specId
 * @property string $specialization
 *
 * @property Teacher[] $teachers
 */
class Specialization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specialization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
              [['specialization'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9 -]+$/', 'message' => 'אין אפשרות להוסיף תווים מיוחדים בשדה זה, נא הזן אותיות ומספרים בלבד'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'specId' => 'מספר התמחות',
            'specialization' => 'התמחות',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::className(), ['specializationName' => 'specId']);
    }

    public static function getSpecializationName() /// It's not working if the function is not ststic function!! 
    {
		$allUserRole = self::find()->all();
		$allUserRoleArray = ArrayHelper::
					map($allUserRole, 'specId', 'specialization');
		return $allUserRoleArray;						
	}
 

		public static function getSpecializationWithAllAspecializations()   //////////////// new function - necessary for dropdown 
	{
		$allUserRole = self::getSpecializationName();
		$allUserRole[-1] = 'כל ההתמחויות';
		$allUserRole = array_reverse ( $allUserRole, true );
		return $allUserRole;	
	}

}
