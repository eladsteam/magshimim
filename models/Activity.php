<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\Student;
use app\models\Useractivity;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * This is the model class for table "activity".
 *
 * @property integer $activityId
 * @property string $activityName
 * @property string $activityDate
 * @property string $day
 * @property string $startTime
 * @property string $endTime
 * @property integer $activityTypeId
 * @property integer $classroomId
 * @property string $comment
 *
 * @property Classroom $classroom
 * @property Activitytype $activityType
 * @property Studentactivity[] $studentactivities
 * @property Useractivity[] $useractivities
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activityDate', 'startTime', 'endTime'], 'safe'],
            [['activityTypeId', 'classroomId'], 'integer'],
            //['comment', 'required'],
            [['activityName', 'day', 'comment'], 'string', 'max' => 255], 
            [['classroomId'], 'exist', 'skipOnError' => true, 'targetClass' => Classroom::className(), 'targetAttribute' => ['classroomId' => 'classroomId']],
            [['activityTypeId'], 'exist', 'skipOnError' => true, 'targetClass' => Activitytype::className(), 'targetAttribute' => ['activityTypeId' => 'activityTypeId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activityId' => 'מספר פעילות',
            'activityName' => 'שם פעילות',
            'activityDate' => 'תאריך',
            'day' => 'יום',
            'startTime' => 'שעת התחלה',
            'endTime' => 'שעת סיום',
            'activityTypeId' => 'סוג פעילות',
            'classroomId' => 'כיתה',
            'comment' => 'הערה',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassroom()
    {
        return $this->hasOne(Classroom::className(), ['classroomId' => 'classroomId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityType()
    {
        return $this->hasOne(Activitytype::className(), ['activityTypeId' => 'activityTypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentactivities()
    {
        return $this->hasMany(Studentactivity::className(), ['activityId' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseractivities()
    {
        return $this->hasMany(Useractivity::className(), ['activityId' => 'activityId']);
    }
   
///////////////// function for converting multi choise array to String in Activity/index.php
    public function getUsersnames(){
        $str = $this->comment;
        $arr = explode(",", $str);
        $arrLength = count($arr);
        for($i=0;$i<=$arrLength-1; $i++){
            $id = intval($arr[$i]);
            $name = \app\models\User::findOne($id)->userName; // This function in User.php
            $arr[$i] = $name;
        }
        $names = implode(", ",$arr); //  ', ' will be between the names
       return $names;

    }

    /////////////////// before save function for converting array names to String
    // public function beforeSave($insert)
    // {
    //     if (parent::beforeSave($insert)) {
    //         $pupilArray = $_POST['Activity']['comment'];
    //         $pupilString = implode(",",array_values($pupilArray));
    //        $_POST['Activity']['comment']= $pupilString;
    //        $this->comment = $pupilString;
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    ///////////////// function for multi choise in Activity form
    public static function getActivityUsersNames()
    {
         $pupil =  ArrayHelper::map(User::find()->all(),'userNumber', 'userName');
        return $pupil;
    }
   
    ///////////////// function for multi choise in Activity form
    public static function getActivityStudentsNames()
    {
         $pupil =  ArrayHelper::map(Student::find()->all(),'studentId', 'nickName');
        return $pupil;
    }
 
    public static function getInitUsersNames($id) //This function get activityId and return an array of the existing users on this activity
    {
        $useractivity = ArrayHelper::map(Useractivity::find()
        ->where(['activityId'=>$id])->all(),'userNumber', 'userNumber');
        return $useractivity;
    }

    public static function getInitStudentsNames($id) //This function get activityId and return an array of the existing students on this activity
    {
        $studentactivity = ArrayHelper::map(Studentactivity::find()
        ->where(['activityId'=>$id])->all(),'studentId', 'studentId');
        return $studentactivity;
    }

    // public function getMarkets() {
    //     return $this->hasMany(Market::className(), ['id' => 'market_id'])
    //     ->viaTable('tbl_user_market', ['user_id' => 'id']);
    // }

    //  public function getUsers() {
    //     return $this->hasMany(User::className(), ['userNumber' => 'userNumber'])
    //     ->viaTable('useractivity', ['activityId' => 'activityId']);
    // }  

    public static function getTeames3($id)  
	{
		$allTeames = (new \yii\db\Query())
           ->select(['*'])
           ->from('useractivity')
           ->where(['activityId' => $id])
           ->all();
		// $allTeamesArray = ArrayHelper::
		// 			map($allTeames, 'userNumber', 'userName');
		return $allTeames;						
	}

    // public static function deleteUseractivity($id)   // /test for delete data from useractivity
	// {
	// 	//$SQL= (new \yii\db\Query())->DELETE * FROM ('useractivity' WHERE activityId = $id ;
    //     //DELETE FROM `useractivity` WHERE activityId= '8'
    //     \Yii::$app
    //     ->db
    //     ->createCommand()
    //     ->delete('useractivity', ['activityId' => $id])
    //     ->execute();
           			
	// }


}
