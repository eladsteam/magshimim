<?php
 
namespace app\models;
use Yii;
use yii\base\model;
 
class FormRecoverPass extends model{
 
    public $email;
     
    public function rules()
    {
        return [
            ['email', 'required', 'message' => 'שדה נדרש'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'כתובת דואר אלקטרוני חייבת להכיל לפחות 5 תווים'],
            ['email', 'email', 'message' => 'פורמט לא חוקי'],
        ];
    }
}