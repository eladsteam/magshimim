<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subsidiary;

/**
 * SubsidiarySearch represents the model behind the search form about `app\models\Subsidiary`.
 */
class SubsidiarySearch extends Subsidiary
{
    /**
     * @inheritdoc
     */
 public $user; // New attribute for filter instead of fk userNumber. there is changes at rules(), search(), andFilterWhere()

    public function rules()
    {
        return [
            [['userNumber', 'teachingHours' ,'suspendHours'], 'integer'],
            [[ 'user'], 'safe'], // New attribute for filter instead of fk userNumber
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subsidiary::find();
        $query->joinWith(['userNumber0']); /// nessecary for filter instead of fk userNumber .userNumber0 is the function that connect teacher and user
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['user'] = [ // nessecary for filter instead of fk userNumber. the attributes are for sort
        'asc' => ['user.firstName' => SORT_ASC],
        'desc' => ['user.lastName' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'userNumber' => $this->userNumber,
            'teachingHours' => $this->teachingHours,
            'suspendHours' => $this->suspendHours,
        ]);

        $query->andFilterWhere(['like', 'user.firstName' ,  $this->user]); //// nessecary for filter instead of fk userNumber. filter by first name

        return $dataProvider;
    }
}
