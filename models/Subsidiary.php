<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subsidiary".
 *
 * @property integer $userNumber
 * @property integer $teachingHours
 * @property integer $suspendHours
 */
class Subsidiary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subsidiary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teachingHours', 'suspendHours'], 'integer'],
            [['userNumber'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userNumber' => 'userNumber']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userNumber' => 'שם בת השירות',
            'teachingHours' => 'שעות הוראה',
            'suspendHours' => 'שעות שהייה',
        ];
    }

    public function getUserNumber0()
    {
        return $this->hasOne(User::className(), ['userNumber' => 'userNumber']);
    }
}
