<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "studentactivity".
 *
 * @property integer $studentId
 * @property integer $activityId
 *
 * @property Activity $activity
 * @property Student $student
 */
class Studentactivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'studentactivity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['studentId', 'activityId'], 'required'],
            [['studentId', 'activityId'], 'integer'],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activityId' => 'activityId']],
            [['studentId'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['studentId' => 'studentId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'studentId' => 'מספר סטודנט',
            'activityId' => 'מספר פעילות',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['activityId' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['studentId' => 'studentId']);
    }
}
