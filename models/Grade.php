<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use app\models\Student;

/**
 * This is the model class for table "grade".
 *
 * @property integer $gradeId
 * @property string $gradeName
 *
 * @property Student[] $students
 */
class Grade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gradeName'], 'match', 'pattern' => '/^[a-zA-Zא-ת0-9 -]+$/', 'message' => 'אין אפשרות להוסיף תווים מיוחדים בשדה זה, נא הזן אותיות ומספרים בלבד'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gradeId' => 'מספר כיתה',
            'gradeName' => 'כיתה',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['grade' => 'gradeId']);
    }
	
		////////////////
		public static function getStudentGrade() ///////////////////// new function for dropdown
	{
		$allUserRole = self::find()->all();
		$allUserRoleArray = ArrayHelper::
					map($allUserRole, 'gradeId', 'gradeName');
		return $allUserRoleArray;						
	}

		public static function getStudentGradeWithAllGrades()   //////////////// new function - necessary for dropdown 
	{
		$allUserRole = self::getStudentGrade();
		$allUserRole[-1] = 'כל הכיתות';
		$allUserRole = array_reverse ( $allUserRole, true );
		return $allUserRole;	
	}
	////////////////////////////
}
