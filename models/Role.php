<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use app\models\User;

/**
 * This is the model class for table "role".
 *
 * @property integer $roleId
 * @property string $roleType
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roleType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'roleId' => 'מספר תפקיד',
            'roleType' => 'סוג תפקיד',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['roleName' => 'roleId']);
    }
	
	////////////////
		public static function getUserRole() ///////////////////// new function for dropdown
	{
		$allUserRole = self::find()->all();
		$allUserRoleArray = ArrayHelper::
					map($allUserRole, 'roleId', 'roleType');
		return $allUserRoleArray;						
	}

		public static function getUserRoleWithAllRoles()   //////////////// new function - necessary for dropdown 
	{
		$allUserRole = self::getUserRole();
		$allUserRole[-1] = 'כל התפקידים';
		$allUserRole = array_reverse ( $allUserRole, true );
		return $allUserRole;	
	}
	////////////////////////////
}
