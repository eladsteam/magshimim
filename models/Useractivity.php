<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "useractivity".
 *
 * @property integer $userNumber
 * @property integer $activityId
 *
 * @property Activitychange[] $activitychanges
 * @property Activity $activity
 * @property User $userNumber0
 */
class Useractivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'useractivity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userNumber', 'activityId'], 'required'],
            [['userNumber', 'activityId'], 'integer'],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activityId' => 'activityId']],
            [['userNumber'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userNumber' => 'userNumber']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userNumber' => 'מספר משתמש',
            'activityId' => 'מספר פעילות',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivitychanges()
    {
        return $this->hasMany(Activitychange::className(), ['teacherOut' => 'userNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['activityId' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNumber0()
    {
        return $this->hasOne(User::className(), ['userNumber' => 'userNumber']);
    }
}
