<?php
 
namespace app\models;

use Yii;
use yii\base\model;
 
class FormResetPass extends model{
 
 public $email;
 public $password;
 public $password_repeat;
 public $verification_code;
 //public $recover;
     
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'verification_code'/*, 'recover'*/], 'required', 'message' => 'שדה נדרש'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'תווים לפחות 5 ומקסימום 80'],
            ['email', 'email', 'message' => 'פורמט לא חוקי'],
            ['password', 'match', 'pattern' => "/^.{4,16}$/", 'message' => 'הסיסמה חייבת להיות לפחות 4 תווים ומקסימום 16 תווים'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'הסיסמאות אינן תואמות'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'דואר אלקטרוני',
            'password' => 'סיסמה חדשה',
            'password_repeat' => 'סיסמה חדשה בשנית',
            'verification_code' => 'קוד אימות',
            ];
    }
}