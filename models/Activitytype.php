<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "activitytype".
 *
 * @property integer $activityTypeId
 * @property string $activityTypeName
 *
 * @property Activity[] $activities
 */
class Activitytype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activitytype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activityTypeName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activityTypeId' => 'מספר סוג פעילות',
            'activityTypeName' => 'סוג פעילות',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['activityTypeId' => 'activityTypeId']);
    }

    ////////////////
		public static function getActivityType() ///////////////////// new function for dropdown
	{
		$allUserRole = self::find()->all();
		$allUserRoleArray = ArrayHelper::
					map($allUserRole, 'activityTypeId', 'activityTypeName');
		return $allUserRoleArray;						
	}

    	public static function getActivityTypeWithAllTypes()   //////////////// new function - necessary for dropdown 
	{
		$allUserRole = self::getActivityType();
		$allUserRole[-1] = 'All Types';
		$allUserRole = array_reverse ( $allUserRole, true );
		return $allUserRole;	
	}
	////////////////////////////
}
