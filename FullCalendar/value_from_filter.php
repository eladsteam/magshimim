<?php

session_start(); // This session is for reading __id session from yii -> We need this session for default filter
	if (empty($_SESSION['__id'])) { // If there is no active session
		$identity= "I'm soory, No identity session   ";
		$authorizationLevel = "No Authorization!!!";
	}
	else{ // If there is identity active session that came from yii
		$identity = $_SESSION['__id']; // __id session came from yii
		$sqlForAuthoriaztion = "SELECT `item_name` FROM `auth_assignment` WHERE `user_id`=$identity"; // Check the authorization of the user -> We need the authorization level for filter default value 
		$reqForAuthoriaztion = $bdd->prepare($sqlForAuthoriaztion); // $sql came from value_from_filter.php
		$reqForAuthoriaztion->execute();
		$eventsForAuthoriaztion = $reqForAuthoriaztion->fetch();
		$valueForAuthoriaztion =  $eventsForAuthoriaztion[0];
		if($valueForAuthoriaztion== "כל ההרשאות"){
			$authorizationLevel = '1'; // full authorization -> CRUD
		}
		else{
			$authorizationLevel = '2'; // half authorization -> View Only
		}
	}

	if(isset($_GET['valueToSearch']) && $_GET['titleToSearch'] && $_GET['valueToSearch'] !=0 && $_GET['titleToSearch'] !=0) // 0 is in case that the user choose "all options" in filter dropdown 
	{	 // In case the user selected proper filtering
		$titleToSearch =  $_GET['titleToSearch'];
		$valueToSearch = $_GET['valueToSearch']; 
		$filterFlag = "1"; // this flag is for hide/show the unfilter button
		// The second query in every "if" is for the second filter		
		if($titleToSearch == '1') // If the user filtered by classromm
		{
			// 1.Filter - here we get the value that user want filering
			$sql = "SELECT activityId, activityName, start, end, color, activityTypeId, classroomId, comment, addNumber, usersString FROM activity WHERE classroomId=$valueToSearch";
			$firstFilterValue = "כיתת לימוד";
			$sqlValueToSearch = "SELECT `classroomName` FROM `classroom` WHERE `classroomId`=$valueToSearch";
		}

		elseif($titleToSearch == '2') // If the user filtered by activity type
		{
			$sql = "SELECT activityId, activityName, start, end, color, activityTypeId, classroomId, comment, addNumber, usersString FROM activity WHERE activityTypeId=$valueToSearch";
			$firstFilterValue = "סוג פעילות";
			$sqlValueToSearch = "SELECT `activityTypeName` FROM `activitytype` WHERE `activityTypeId`=$valueToSearch";
		}

		elseif($titleToSearch == '3') // If the user filtered by user
		{
			$sql = "SELECT A.activityId, A.activityName, A.start, A.end, A.color, A.activityTypeId, A.classroomId, A.comment, A.addNumber, A.usersString FROM activity A, useractivity U WHERE A.activityId=U.activityId AND U.userNumber=$valueToSearch";
			$firstFilterValue = "איש צוות";
			$sqlValueToSearch = "SELECT `userName` FROM `user` WHERE `userNumber`=$valueToSearch";
		}

		elseif($titleToSearch == '4') // If the user filtered by student
		{
			$sql = "SELECT A.activityId, A.activityName, A.start, A.end, A.color, A.activityTypeId, A.classroomId, A.comment, A.addNumber, A.usersString FROM activity A, studentactivity S WHERE A.activityId=S.activityId AND S.studentId=$valueToSearch";
			$firstFilterValue = "תלמיד";
			$sqlValueToSearch = "SELECT `nickName` FROM `student` WHERE `studentId`=$valueToSearch";
		}

		elseif($titleToSearch == '5') // If the user filtered by grade
		{
			$sql = "SELECT DISTINCT A.activityId, A.activityName, A.start, A.end, A.color, A.activityTypeId, A.classroomId, A.comment, A.addNumber, A.usersString FROM activity A, grade G, student S, studentactivity U WHERE S.grade=G.gradeId AND S.studentId=U.studentId AND U.activityId=A.activityId AND G.gradeId=$valueToSearch";
			$firstFilterValue = "כיתה";
			$sqlValueToSearch = "SELECT `gradeName` FROM `grade` WHERE `gradeId`=$valueToSearch";
		}
	}

	else{  // In case the user dosn't selected proper filtering -> print default values
		if($authorizationLevel== "1"){ // Admin will see all the activities as default
			$sql = "SELECT activityId, activityName, start, end, color, activityTypeId, classroomId, comment, addNumber, usersString FROM activity WHERE activityTypeId != '4' AND activityTypeId != '2' "; // remove the vacation and staing time from default view
		}
		else{ // Other user will see only his activities as default and can't see all the activities together 
			$sql = "SELECT A.activityId, A.activityName, A.start, A.end, A.color, A.activityTypeId, A.classroomId, A.comment, A.addNumber, A.usersString FROM activity A, useractivity U WHERE A.activityId=U.activityId AND U.userNumber=$identity";
		}	
			$firstFilterValue = "חיפוש לפי"; // value for first filter
			$sqlValueToSearch = "0";
			$filterFlag = "0"; // this flag is for hide/show the unfilter button
	}




?>