<?php

require_once('bdd.php');

	if (isset($_POST['delete']) && isset($_POST['activityId'])){

		$activityId = $_POST['activityId'];
		$deleteVal = $_POST['delete'];
		
		if ($deleteVal == '1'){ 

		$sql = "DELETE FROM activity WHERE activityId = $activityId";
		$query = $bdd->prepare( $sql );
		if ($query == false) {
		print_r($bdd->errorInfo());
		die ('Erreur prepare delete activity');
		}
		$res = $query->execute();
		if ($res == false) {
		print_r($query->errorInfo());
		die ('Erreur execute delete activity');
		}
}

	if ($deleteVal == '2'){ 

		$activityId = $_POST['activityId'];
		$start1 = $_POST['start'];
		$startNumber1 = strtotime("$start1"); // the date comes wired format, so here we are convert it
		$startNumber1 = $startNumber1-'9000'; // the number must be samller than the number date we want to update
		$start2 = date("Y-m-d H:i:s",$startNumber1); // here we are create the format like our DateTime to compare the if in line 40 (start > '$start2')
		$activityName = $_POST['activityName'];
		$activityTypeId = $_POST['activityTypeId'];
		$classroomId = $_POST['classroomId'];
			if ($classroomId == "")
		{
		$classroomId ='3';

		}
		$addNumber = $_POST['addNumber'];
		$usersString = $_POST['usersString'];
		
		$sql = "DELETE FROM activity WHERE  activityTypeId = $activityTypeId AND addNumber = $addNumber AND classroomId = $classroomId AND start > '$start2'";

		$query = $bdd->prepare( $sql );
		if ($query == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare delete all from activity');
		}
		$res = $query->execute();
		if ($res == false) {
			print_r($query->errorInfo());
			die ('Erreur execute delete all from activity');
		}

	}
	}

	// update activity
	elseif (isset($_POST['activityName']) && isset($_POST['activityId'])  && isset($_POST['activityTypeId'])  && !isset($_POST['delete']) && !isset($_POST['deleteAll']) ){
	
		$activityId = $_POST['activityId'];
		$activityName = $_POST['activityName'];
		$activityTypeId = $_POST['activityTypeId'];
		
		if ($activityTypeId == '1'){ //color condetion
			$color = $_POST['color']; // turquoise;
			$classroomId = $_POST['classroomId']; 
		}
		elseif ($activityTypeId == '2'){ //color condetion
			$color = 	'#7FFFD4'; // greenlight
			$classroomId = '4';
		}
	
		elseif ($activityTypeId == '4'){ //color condetion
			$color = '#FF7F50'; // spaciel orange
			$classroomId = '3';
		}
	
			
		// if ($classroomId == "") dont think it should be (COLORS)
		// {
		// $classroomId ='3';

		// }

			$users = $_POST['users_known']; 
			$students = $_POST['students_known']; 
			$comment = $_POST['comment'];
			$usersString = $_POST['usersString'];
		
			// Update activity table
		$sql = "UPDATE activity SET  activityName = '$activityName', color = '$color', activityTypeId = '$activityTypeId', classroomId = '$classroomId', comment = '$comment' WHERE activityId = $activityId ";
		
		$query = $bdd->prepare( $sql );
		if ($query == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare');
		}
		$sth = $query->execute();
		if ($sth == false) {
			print_r($query->errorInfo());
			die ('Erreur execute');
		}
		
				// Update users -> delete all users in activity and then insert new users
		$deleteUsers = "DELETE FROM useractivity WHERE activityId = $activityId";
		
		$query2 = $bdd->prepare( $deleteUsers );
		if ($query2 == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare delete from useractivity');
		}
		$sth2 = $query2->execute();
			if ($sth2 == false) {
			print_r($query2->errorInfo());
			die ('Erreur execute delete from useractivity');
		}

			// Convert users into int array
		for( $i =0; $i < count( $users ); $i++ )
		{    
			$c[$i] = (int) $users[$i];
		}

			// Insertion to useractivity table
		for($test = 0; $test < count($c); $test++)
		{
			$userName=$c[$test];
			$insertToUseractivity = "INSERT INTO useractivity(activityId, userNumber) VALUES ('$activityId','$userName')";
			
			$insertToUseractivitys = $bdd->prepare( $insertToUseractivity ); // We must this line to insert values into table!!!!!
			if ($insertToUseractivitys == false) {
				print_r($bdd->errorInfo());
				die ('Erreur prepare insert to useractivity table');
			}
			
			$insertToUseractivityTable = $insertToUseractivitys->execute(); // We must this line to insert values into table!!!!!
			if ($insertToUseractivityTable == false) {
				print_r($insertToUseractivitys->errorInfo());
				die ('Erreur execute insert to useractivity table');
			}

			/////// we want query for convert userNumber to userName
			$converIdToName="SELECT `userName` FROM `user` WHERE `userNumber`= '$userName'";
			$converIdToName1 = $bdd->prepare($converIdToName); 
			$converIdToName1->execute();
			$converIdToName2 = $converIdToName1->fetch(); // names is 2 arrays and we need to execute them
			$converIdToName2 =  $converIdToName2[0];
			$usersNamesArray[$test] = $converIdToName2;
		
		}

		/// convert users names array to string and insert to activity table as a string
		$usersString = implode(",", $usersNamesArray); 

		$sql = "UPDATE `activity` SET `usersString`= '$usersString' WHERE `activityId`= '$activityId' ";
		
		$query = $bdd->prepare( $sql );
		if ($query == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare update usersString to activity table');
		}
		$sth = $query->execute();
		if ($sth == false) {
			print_r($query->errorInfo());
			die ('Erreur execute update usersString to activity table');
		}
			/// End of update users

				// Update students -> delete all students in activity and then insert new students
		$deleteStudents = "DELETE FROM studentactivity WHERE activityId = $activityId";
		
		$query3 = $bdd->prepare( $deleteStudents );
		if ($query3 == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare delete from studentactivity');
		}
		$sth3 = $query3->execute();
		if ($sth3 == false) {
			print_r($query3->errorInfo());
			die ('Erreur execute delete from studentactivity');
		}

				// Convert students into int array
		for( $i =0; $i < count( $students ); $i++ )
		{    
			$d[$i] = (int) $students[$i];
		}
			// Insertion to studentactivity table
		for($test = 0; $test < count($d); $test++)
		{
			$studentName=$d[$test];
			$insertToStudentactivity = "INSERT INTO `studentactivity`(`studentId`, `activityId`) VALUES ('$studentName','$activityId')"; // Insert to studentactivity table
			
			$insertToStudentactivitys = $bdd->prepare( $insertToStudentactivity ); // We must this line to insert values into table!!!!!
			if ($insertToStudentactivitys == false) {
				print_r($bdd->errorInfo());
				die ('Erreur prepare insert to studentactivity table');
			}

			$insertToStudentactivityTable = $insertToStudentactivitys->execute(); // We must this line to insert values into table!!!!!
			if ($insertToStudentactivityTable == false) {
				print_r($insertToStudentactivityTable->errorInfo());
				die ('Erreur execute insert to studentactivity table');
			}
		}
			/// End of update students
		

	} // End of update one activity

	/////////////		update kavua 	/////////////
	if ($deleteVal == '3'){ 

	
		$activityId = $_POST['activityId'];
		$activityName = $_POST['activityName'];
		$activityTypeId = $_POST['activityTypeId'];
			if ($activityTypeId == '1'){ //color condetion
			$color = $_POST['color']; // turquoise;
			$classroomId = $_POST['classroomId']; 
		}
		elseif ($activityTypeId == '2'){ //color condetion
			$color = 	'#7FFFD4'; // greenlight
			$classroomId = '4';
		}
	
		elseif ($activityTypeId == '4'){ //color condetion
			$color = '#FF7F50'; // spaciel orange
			$classroomId = '3';
		}
		
	
		$addNumber = $_POST['addNumber'];
		$users = $_POST['users_known']; 
		$students = $_POST['students_known'];
		$comment = $_POST['comment'];
		$activityId = $_POST['activityId'];
		$start1 = $_POST['start'];
		$usersString = $_POST['usersString'];
		$startNumber1 = strtotime("$start1"); // the dat comes wired format, so here we are convert it
		$startNumber1 = $startNumber1-'9000'; // the number must be samller than the number date we want to update
		$start2 = date("Y-m-d H:i:s",$startNumber1);

			// Update activity table
		$sql = "UPDATE activity SET  activityName = '$activityName', color = '$color', activityTypeId = '$activityTypeId', classroomId = '$classroomId', comment = '$comment' WHERE addNumber = $addNumber AND start > '$start2'";
		
		$query = $bdd->prepare( $sql );
		if ($query == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare update kavua activity table');
		}
		$sth = $query->execute();
		if ($sth == false) {
			print_r($query->errorInfo());
			die ('Erreur execute update kavua activity table');
		}

		//////// Number of activities that will change - we need it for insert to useractivity table after delete
		$select = "SELECT COUNT(A.activityId) FROM activity A WHERE A.addNumber= $addNumber AND A.start > '$start2'";
		$selects = $bdd->prepare($select); 
		$selects->execute();
		$names = $selects->fetch(); // names is 2 arrays and we need to execute them
		$activityNumberCount =  $names[0]; // This is the first execute
		
			// Update users -> delete all users in activity and then insert new users
		$deleteUsers = "DELETE FROM `useractivity` WHERE useractivity.activityId IN (SELECT activityId FROM activity WHERE addNumber=$addNumber AND start > '$start2')";

		$query2 = $bdd->prepare( $deleteUsers );
		if ($query2 == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare delete kavua delete from useractivity');
		}
		$sth2 = $query2->execute();
		if ($sth2 == false) {
			print_r($query2->errorInfo());
			die ('Erreur execute delete kavua from useractivity');
		}
		

			// Convert users into int array
		for( $i =0; $i < count( $users ); $i++ )
		{    
			$e[$i] = (int) $users[$i];
		}

					// Update students -> delete all students in activity and then insert new students
		$deleteStudents = "DELETE FROM `studentactivity` WHERE studentactivity.activityId IN (SELECT activityId FROM activity WHERE addNumber=$addNumber AND start > '$start2')";
		
		$query3 = $bdd->prepare( $deleteStudents );
		if ($query3 == false) {
			print_r($bdd->errorInfo());
			die ('Erreur prepare delete kavua delete from studentactivity');
		}
		
		$sth3 = $query3->execute();
		if ($sth3 == false) {
			print_r($query3->errorInfo());
			die ('Erreur execute delete kavua from studentactivity');
		}

				// Convert students into int array
		for( $i =0; $i < count( $students ); $i++ )
		{    
			$f[$i] = (int) $students[$i];
		}

		for( $j=0; $j<$activityNumberCount; $j++){ // loop for changing activityId and insert to useractivity -> many activities that we edit

			$activityExistOrNotFlag = true;	// Boolean flag for stay or leave this while loop
			while($activityExistOrNotFlag){
				$resultExistId = "SELECT activityId FROM activity WHERE activityId='$activityId'";// We want to check if this activityId exist or the user delete it
				$selectsResultExistId = $bdd->prepare($resultExistId); 
				$selectsResultExistId->execute();
				$activityExistOr = $selectsResultExistId->fetch(); // names is 2 arrays and we need to execute them
				$activityExistOrNot =  $activityExistOr[0];

				if(empty($activityExistOrNot)){
					$activityId++; // If this activity dosn't exist -> up the activityId 1 more
				}
				else{
					$activityExistOrNotFlag = false; // If this activity exist -> exit from this loop
				}
			} // End of while loop

			// Insertion to useractivity table
			if (count($users)>0){ //this condition check if users is null
			for($test = 0; $test < count($e); $test++)
			{
				$userName=$e[$test];
				$insertToUseractivity = "INSERT INTO useractivity(activityId, userNumber) VALUES ('$activityId','$userName') ";
				
				$insertToUseractivitys = $bdd->prepare( $insertToUseractivity ); // We must this line to insert values into table!!!!!
				if ($insertToUseractivitys == false) {
					print_r($bdd->errorInfo());
					die ('Erreur prepare insert kavua useravticity table');
				}
				
				$insertToUseractivityTable = $insertToUseractivitys->execute(); // We must this line to insert values into table!!!!!
				if ($insertToUseractivityTable == false) {
					print_r($insertToUseractivitys->errorInfo());
					die ('Erreur execute insert kavua useravticity table');
				}

				/////// we want query for convert userNumber to userName
			$converIdToName="SELECT `userName` FROM `user` WHERE `userNumber`= '$userName'";
			$converIdToName1 = $bdd->prepare($converIdToName); 
			$converIdToName1->execute();
			$converIdToName2 = $converIdToName1->fetch(); // names is 2 arrays and we need to execute them
			$converIdToName2 =  $converIdToName2[0];
			$usersNamesArray[$test] = $converIdToName2;
			
			} /// End of update users

				/// convert users names array to string and insert to activity table as a string
					
			$usersString = implode(",", $usersNamesArray); 

			$sql = "UPDATE `activity` SET `usersString`= '$usersString' WHERE `activityId`= '$activityId' ";
			
			$query = $bdd->prepare( $sql );
			if ($query == false) {
				print_r($bdd->errorInfo());
				die ('Erreur prepare update kavua usersString to activity table');
			}
			$sth = $query->execute();
			if ($sth == false) {
				print_r($query->errorInfo());
				die ('Erreur execute update kavua usersString to activity table');
			}

			} //here condition check users null is done


			// Insertion to studentactivity table
			if (count($students)>0){ //this condition check if students is null
			for($test = 0; $test < count($f); $test++)
			{
				$studentName=$f[$test];
				$insertToStudentactivity = "INSERT INTO `studentactivity`(`activityId`, `studentId`) VALUES ('$activityId','$studentName')"; // Insert to studentactivity table
				
				$insertToStudentactivitys = $bdd->prepare( $insertToStudentactivity ); // We must this line to insert values into table!!!!!
				if ($insertToStudentactivitys == false) {
					print_r($bdd->errorInfo());
					die ('Erreur prepare insert kavua studentactivity table');
				}

				$insertToStudentactivityTable = $insertToStudentactivitys->execute(); // We must this line to insert values into table!!!!!
				if ($insertToStudentactivityTable == false) {
					print_r($insertToStudentactivityTable->errorInfo());
					die ('Erreur execute insert kavua studentactivity table');
				}
			} /// End of update students

			} //here condition check students count null is done
					
		$activityId++; // ++ is for update activityId with 1 more
		// End of for loop - loop for changing activityId and insert to useractivity -> many activities that we edit
		
		}
}

header('Location: index.php');
	
?>