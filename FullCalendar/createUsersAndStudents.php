<?php
// This file are getting values from index.php and returning to index.php while creating new activity
include "bdd.php";
include "db.php"; 
?>
<head>	

	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<!--<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">-->
	<!-- We don't want to use the line above because than it will change the botstrap in every update -->
	<!--<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>--> 
	<!-- We don't want to use the line above because than it will be include twice -->
	<link rel="stylesheet" href="dist/fastselect.min.css">
	<script src="dist/fastselect.standalone.js"></script>

	<style>
			.fstElement { font-size: 1.2em; }
			.fstToggleBtn { min-width: 16.5em; }
			.submitBtn { display: none; }
			.fstMultipleMode { display: block; }
			.fstMultipleMode .fstControls { width: 100%; }
	</style>

</head>

<body>
	
<?php
if (isset($_POST['Event'][0]) && isset($_POST['Event'][1]) ){ // $_POST['Event'] came from index.php in the select function through ajax

	$start = $_POST['Event'][0];
	$end = $_POST['Event'][1];
  
  ?>
        <label for="users" class="col-sm-2 control-label">אנשי צוות</label>
					<div class="col-sm-10">
					<select class="multipleSelect" name="users_known[]" multiple name="language">
					<?php
					$allAvailableUsers = mysql_query("SELECT `userNumber`, `userName` FROM `user` WHERE `userNumber` NOT IN (SELECT DISTINCT U.userNumber FROM user U, useractivity UA, activity A WHERE U.userNumber=UA.userNumber AND UA.activityId = A.activityId AND UA.activityId IN (SELECT `activityId` FROM `activity` WHERE start <= '$start' AND end >= '$end'))");
					// $allAvailableUsers = all users that not in other activity at the same time or less. this query will show user even if there is at least 1 minutes that he is fre
					$i=0;
					while($userFromList = mysql_fetch_array($allAvailableUsers)) {
						?>
						<option value="<?=$userFromList["userNumber"];?>"><?=$userFromList["userName"];?></option>
						<!-- 	We want to display userName but to send userNumber  -->
						<?php
						$i++;
					} ?>
					</select>
						<script>
									$('.multipleSelect').fastselect(); //script code for multiple select
						</script>
					</div>
				  </div>

<br>
<br>
<br>

			<label for="students" class="col-sm-2 control-label">תלמידים</label>
					<div class="col-sm-10">
					<select class="multipleSelect" name="students_known[]" multiple name="language">
					<?php
					$allAvailableStudents = mysql_query("SELECT `studentId`, `nickName` FROM `student` WHERE `studentId` NOT IN (SELECT DISTINCT S.studentId FROM student S, studentactivity SA, activity A WHERE S.studentId=SA.studentId AND SA.activityId = A.activityId AND SA.activityId IN (SELECT `activityId` FROM `activity` WHERE start <= '$start' AND end >= '$end')) ORDER BY grade");
					// $allAvailableStudents = all students that not in other activity at the same time or less. this query will show user even if there is at least 1 minutes that he is fre
					$i=0;
					while($studentFromList = mysql_fetch_array($allAvailableStudents)) {
						?>
						<option value="<?=$studentFromList["studentId"];?>"><?=$studentFromList["nickName"];?></option>
						<!-- 	We want to display student nickName but to send studentId  -->
						<?php
						$i++;
					} ?>
					</select>
						<script>
									$('.multipleSelect').fastselect(); //script code for multiple select
						</script>
					</div>
				  </div>


				  

 <?php 
  }
 ?>


 
 </body>