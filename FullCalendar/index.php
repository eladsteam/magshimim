<?php
header('Content-Type: text/html; charset=utf-8'); 
require_once('bdd.php');
include "db.php"; // Elad's db file
include "connectdb.php"; // Nisim's db file
include "value_from_filter.php";   // This file contains the value from filter and $sql

$req = $bdd->prepare($sql); // $sql and authorization level came from value_from_filter.php
$req->execute();
$events = $req->fetchAll();

		// The "if" end "else" are for the filter
// $sqlValueToSearch came from value_from_filter.php
	if($sqlValueToSearch=="0"){ // In case the user didn't select nothing in the second filter
		$secondFilterValue = "נתון"; // This variable goes to the filter
		$chosen = ""; // This variable goes to the filter
	}
	else{ // $selectSqlValueToSearch came from value_from_filter.php
		$selectSqlValueToSearch = $bdd->prepare($sqlValueToSearch);  // In case the user selected something in the second filter
		$selectSqlValueToSearch->execute();
		$selectsSqlValueToSearch = $selectSqlValueToSearch->fetch(); 
		$secondFilterValue =  $selectsSqlValueToSearch[0];
		$chosen = "בחירה: "; // This variable goes to the filter	
	}

?>

<!DOCTYPE html>
<!-- clock widget start -->
<!-- clock widget end -->

<html lang="en">

<head>


<meta charset="utf-8">	
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta http-equiv="x-ua-compatible" content="ie=edge">


<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">
<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
<link rel="stylesheet" href="dist/fastselect.min.css">
<link rel="stylesheet" href="dist/fastradio.css"> <!-- radio button -->
<script src="dist/fastselect.standalone.js"></script>


<script>
function showUser(str) {
 
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("POST","index.php?q="+str,true);
        xmlhttp.send();
    }
}
</script>

<style>
	.fstElement { font-size: 1.2em; }
	.fstToggleBtn { min-width: 16.5em; }
	.submitBtn { display: none; }
	.fstMultipleMode { display: block; }
  .fstMultipleMode .fstControls { width: 100%; }
 </style>
	

</head>


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Calendar</title>

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
	
	<!-- FullCalendar -->
	<link href='css/fullcalendar.css' rel='stylesheet' />

	<!-- Files for nice alert message (sweetAlert) while drop activity, The files and moe example are from http://t4t5.github.io/sweetalert/ -->
	<script src="dist/sweetalert.min.js"></script>
	<link rel="stylesheet" type="text/css" href="dist/sweetalert.css">

  <!-- Custom CSS -->
  <style>
			 /*	style for calendar  */
    body 
		{		
    padding-top: 0px;
		padding-right: 0px;
		padding-left: 0px;
		margin-right:0px;
		margin-left:0px;	
		width: 100%;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
	#calendar {
		width: 100%;
		
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}
  </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<body>

<!--<form action="index.php" method="get">
					<div class="form-group">
					<label for="classroomId" class="col-sm-2 control-label">כיתה</label>
					<div class="col-sm-10">
						<select id="classroomId" class="form-control"  name="valueToSearch">
							php here!!!
							$mysqlserver="localhost";
							$mysqlusername="root";
							$mysqlpassword="";
							$link=mysql_connect(localhost, $mysqlusername, $mysqlpassword) or die ("Error connecting to mysql server: ".mysql_error());
							$dbname = 'magshimim';
							mysql_select_db($dbname, $link) or die ("Error selecting specified database on mysql server: ".mysql_error());
							mysql_query("SET NAMES 'utf8'",$link); // Generate utf8 for hebrew
							$sql = "SELECT classroomId, classroomName FROM classroom";
							$result = mysql_query($sql);

							
							while ($row = mysql_fetch_array($result)) {
							echo "<option value='" . $row['classroomId'] ."'>" . $row['classroomName'] ."</option>";
							}
					?>
        		</select>
					</div>
				  </div>
</form>
<input type="submit" name="search" value="חפש">-->


<!-- ajax of th multiple search PART2 -->

<script type="text/javascript">
function getData(val) 	// This function get value from filter, send it to get_filter_data.php and return result for the second filter
{
	$.ajax({
		type:"POST",
		url:"get_filter_data.php",
		data: 'fatherFilterValue='+val,	
		success: function(data){
			$("#mainselection").html(data); // mainselection is the filter id
		}});

}

</script>

<!-- ajax PART2 done here-->

<!-- MULTIPLE SEARCH PART 1 -->



	<!-- 	Filter- get values from getData function and pass it to index.php through value_from_filter.php	-->
<form action="index.php" method="get">
<div id="filter" style="float:right;margin-right:50px;">
<!--<label>:נתון </label>-->
<select id="mainselection" name="valueToSearch" dir="rtl">
<!-- $secondFilterValue & chosen declared in the begining of this page	-->
<option value="0"><?php echo $secondFilterValue ?></option>  
</select>
<!--<label>:חפש לפי </label>-->
<select id="mainselection"  name="titleToSearch" onChange="getData(this.value)" dir="rtl">
<!-- $firstFilterValue declared in value_from_filter.php	-->
<option value="0"><?php echo $chosen . "" .$firstFilterValue ?></option> 
<option value="1">כיתת לימוד</option>
<option value="2">סוג פעילות</option>
<option value="3">איש צוות</option>
<option value="4">תלמיד</option>
<option value="5">כיתה</option>
</select>
<br>
</div>
<div>
<button value="submit" class="filter">סנן</button>
</div>
<div>
<!-- $filterFlag came from "value_from_filter.php" and represent hat filter is on/off and show/hide the unfilter button	-->
<input type="button" class="unfilter" value="נקה סינון" onclick="location.href = 'http://localhost/magshimim/fullcalendar/'"   <?php if($filterFlag==0) {?> hidden <?php } ?> >
</div>
</form>	

 <!-- MULTIPLE SEARCH PART 1 done here-->



 
  <!-- 4(LAST) FILTER - here html botton and input send as POST up to function 1/2/3 

<form>
<select name="users" onchange="showUser(this.value)">
  <option value="">Select a person:</option>
  <option value="1">Peter Griffin</option>
  <option value="2">Lois Griffin</option>
  <option value="3">Joseph Swanson</option>
  <option value="4">Glenn Quagmire</option>
  </select>
</form>
<br>
<div id="txtHint"><b>Person info will be listed here...</b></div>


 FILTER DONE -->

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <!--<h1>FullCalendar BS3 PHP MySQL</h1>-->
                <!--<p class="lead">Complete with pre-defined file paths that you won't have to change!</p>-->
								<br>
								<br>
                <div id="calendar" class="col-centered">
                </div>
            </div>
        </div>
        <!-- /.row -->
				
		
		<!-- Modal --> 
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		
		  <div class="modal-dialog" role="document">
			<div class="modal-content">


			
			<form class="form-horizontal" method="POST" action="addEvent.php">
			
			
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">הוספת פעילות</ht4>
			  </div>
				
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="activityName"class="col-sm-2 control-label">שם</label>
					<div class="col-sm-10">

					  <input type="text" name="activityName" class="form-control" id="activityName" placeholder="שם הפעילות" onkeyup="letterOnly(this)"  required>
					</div>
				  </div>


										<script>
					function letterOnly(input) {
						var regex = /[^א-תa-zA-Z0-9 ]/gi;
						input.value = input.value.replace(regex,"");
					}
					</script>

				  <div class="form-group">
					<label for="activityDate" class="col-sm-2 control-label">שעת התחלה</label>
					<div class="col-sm-10">
					  <input type="text" name="start" class="form-control" id="start" >
						<!--<input type="datetime-local" value="2017-03-24T08:00:00" name="start" class="form-control" id="start" >  We can user date picker but it's not nice-->
					</div>
				  </div>
				  <div class="form-group">
					<label for="end" class="col-sm-2 control-label">שעת סיום</label>
					<div class="col-sm-10">
					  <input type="text" name="end" class="form-control" id="end" >
					</div>
				  </div>
					<div class="form-group">
					<label for="activityTypeId" class="col-sm-2 control-label">סוג פעילות</label>
					<div class="col-sm-10">
        		<select id="activityTypeId" class="form-control"  name="activityTypeId" dir="rtl">
            	<?php
							$result = mysql_query("SELECT activityTypeId, activityTypeName FROM activitytype");
							while ($row = mysql_fetch_array($result)) {
								echo "<option value='" . $row['activityTypeId'] ."'>" . $row['activityTypeName'] ."</option>";
							}
						?>
        	</select>
					<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript"> //2.2 this script works when we want to create activity, it should be because when we want to make "vecation", the droplists of classroom, and quantinty not needed so it is hide
    $(function () {

				    $(function () {
        $("#activityTypeId").change(function () {
            if ($(this).val() == "1") {
                 $("#colorlabel").show();
								 $("#color").show();
								 $("#classroomIdlabel").show();
							   $("#classroomId").show();
						  	 
            } else {
                 $("#colorlabel").hide();
							   $("#color").hide();
								 $("#classroomIdlabel").hide();
							   $("#classroomId").hide();

								
            }
        });
    }); //script 2..2 done
    }); //script 2..2 done
</script>
					</div>
				  </div>
					<div class="form-group">
					<label for="color" id="colorlabel" class="col-sm-2 control-label">צבע</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
						  <option value="">בחר צבע</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; כחול</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; טורקיז</option>
						  <option style="color:#008000;" value="#008000">&#9724; ירוק</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; צהוב</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; כתום</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; אדום</option>
						  <option style="color:#000;" value="#000">&#9724; שחור</option>
						</select>
					</div>
				  </div>

					<div class="form-group">
					<label for="classroomId" id="classroomIdlabel" class="col-sm-2 control-label">כיתה</label>
					<div class="col-sm-10">
						<select id="classroomId" class="form-control"  name="classroomId" dir="rtl">
							<?php
							$result = mysql_query("SELECT classroomId, classroomName FROM classroom WHERE classroomId != '3' AND classroomId != '4'"); // not show vecation row at this option
							while ($row = mysql_fetch_array($result)) {
								echo "<option value='" . $row['classroomId'] ."'>" . $row['classroomName'] ."</option>";
							}
							?>
        		</select>
					</div>
				  </div>	

					
									<!--   Create users and students activity Multiple Select - all the fields are located in createUsersAndStudents.php  -->
					<div class="form-group" id="createUsersAndStudents" ></div>

						
					<div class="form-group">
					<label for="comment" id="endlabel" class="col-sm-2 control-label">פעילות חוזרת עד</label>
					<div class="col-sm-10">
					  <input type="date" name="quantity" class="form-control" id="quantity">
					</div>
				  </div>
					<div class="form-group">
					<label for="comment" class="col-sm-2 control-label">הערות</label>
					<div class="col-sm-10">
					  <input type="text" name="comment" class="form-control" id="comment" onkeyup="letterOnly(this)"  placeholder="..רק אם יש">
					</div>
				  </div>
			</div>	
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">סגור</button>
				<button type="submit" class="btn btn-primary" onclick="myFunction()">שמור פעילות</button>


			  </div>
			</form>
			</div>
		  </div>
		</div>
		
		
		
					<!--- //////////////////////////  Modal for update activity ////////////////////////////////////// -->  
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="editEventTitle.php">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">עריכת פעילות</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="activityName" class="col-sm-2 control-label">שם</label>
					<div class="col-sm-10">
					  <input type="text" name="activityName" class="form-control" id="activityName" placeholder="שם פעילות" onkeyup="letterOnly(this)"  required>
					</div>
				  </div>

												<script>
					function letterOnly(input) {
						var regex = /[^א-תa-zA-Z0-9 ]/gi;
						input.value = input.value.replace(regex,"");
					}
					</script> 					<!--- לא חובה -->  
			
				
					<div class="form-group">
					<label for="activityTypeId" style="display:none" class="col-sm-2 control-label">סוג פעילות</label>
					<div class="col-sm-10">
       			<select style="display:none" id="activityTypeId" class="form-control"  name="activityTypeId" dir="rtl">
							<?php
							$result = mysql_query("SELECT activityTypeId, activityTypeName FROM activitytype");
							while ($row = mysql_fetch_array($result)) {
								echo "<option value='" . $row['activityTypeId'] ."'>" . $row['activityTypeName'] ."</option>";
							}
							?>
        		</select>
					</div>
				  </div>

					<div id = "colorA1">
					<div class="form-group">
					<label for="color" id="colorlabel" class="col-sm-2 control-label">צבע</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
						  <option value="">בחר צבע</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; כחול</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; טורקיז</option>
						  <option style="color:#008000;" value="#008000">&#9724; ירוק</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; צהוב</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; כתום</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; אדום</option>
						  <option style="color:#000;" value="#000">&#9724; שחור</option>
						</select>
					</div>
				  </div>
</div>
	<div id="classA1">
					<div class="form-group">
					<label for="classroomId" id="classroomB" class="col-sm-2 control-label">כיתה</label>
					<div class="col-sm-10">
					  <select id="classroomId" class="form-control"  name="classroomId" dir="rtl">
							<?php
							$result = mysql_query("SELECT classroomId, classroomName FROM classroom WHERE classroomId != '3' AND classroomId != '4'");	// not show vecation row at this option
							while ($row = mysql_fetch_array($result)) {
								echo "<option value='" . $row['classroomId'] ."'>" . $row['classroomName'] ."</option>";
							}
							?>
       			</select>

					</div>
				  </div>
	</div>								

					<!--  activityId   -->

					<input type="hidden" name="activityId" class="form-control" id="activityId">


					<!--   Update users and students activity Multiple Select - all the fields are located in updateUsersAndStudents.php  -->

					<div class="form-group" id="updateUsersAndStudents" ></div>
				

					<div class="form-group">
					<label for="comment" class="col-sm-2 control-label">הערה</label>
					<div class="col-sm-10">
					  <input type="text" name="comment" class="form-control" id="comment" onkeyup="letterOnly(this)"  placeholder="הערה	">
					</div>
				  </div>


					
					<label for="start" class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
					  <input type="hidden" name="start" class="form-control" id="start">
					</div>
				


				
					<label for="addNumber" class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
					  <input type="hidden" name="addNumber" class="form-control" id="addNumber">
					</div>
			

				
					<label for="usersString" class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
					  <input type="hidden" name="usersString" class="form-control" id="usersString">
					</div>
				  
 <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">סגור</button>
				<!--<button type="submit" class="btn btn-primary" onclick="myFunction()">שמור שינויים</button>-->
				<button type="button" class="btn btn-primary updateButtonCalendar" data-toggle="modal" data-target="#updateModalCalendar">עדכון</button>
				<button type="button"  class="btn btn-primary deleteButtonCalendar" data-toggle="modal" data-target="#deleteModalCalendar">מחק</button>

				  <!-- Modal -->

			  </div>
	</div> 
 </div>
</div>


				<div class="modal fade" id="updateModalCalendar" role="dialog">
					<div class="modal-dialog">
					
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">עדכון פעילות</h4>
							</div>
							<div class="modal-body" style="margin-left:250px">
									<button value = "3" name="delete" type="submit" class="btn btn-primary" onclick="myFunction() ">עדכן את כל הפעילויות מסוג זה</button>
									<button type="submit" class="btn btn-primary" onclick="myFunction()"> עדכן פעילות </button>
							
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">סגור</button>
							</div>
						</div>
						
					</div>
				

 	</div>
				<div class="modal fade1" id="deleteModalCalendar" role="dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog">
					
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" >מחיקת פעילות</h4>
							</div>
							<div class="modal-body" style="margin-left:250px">
									
									<button  name="delete"  value="2" type="submit" class="btn btn-primary" onclick="myFunction()">מחק את כל הפעילויות מסוג זה</button>
									<button  name="delete" value="1" type="submit" class="btn btn-primary">מחק פעילות </button>
							</div>


							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">סגור</button>
							</div>
						</div>
						
					</div>
				</div>
			</form>
		
		
	
  </div>







    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<!-- FullCalendar -->
	<script src='js/moment.min.js'></script>
	<script src='js/fullcalendar.min.js'></script>
	<script src='js/he.js'></script> 
		
	
<script type="text/javascript">

	$(document).ready(function() {
		var d = new Date();
		var initialLangCode = 'he';
		defaultDate: d,
		
		$('#calendar').fullCalendar({
			header: {
				left: 'next today prev', //comma shows button without backspace
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			 buttonText: { // Values dor cuttons 
                    today: 'היום',
                    month: 'חודשי',
                    week: 'שבועי',
                    day: 'יומי'
      },
			defaultDate: d,
			defaultView: 'agendaDay', // Default view is now agendaDay instead of month. we can allso use agendaWeek
			lang: initialLangCode,
			<?php if($authorizationLevel == '1'){ //if the user is admin -> authorization Level came from value_from_filter.php
			?>
				editable: true,
				selectable: true,
			<?php }
			else{  // if the user is not admin
			?>
				editable: false,
				selectable: false,
			<?php }	?>
			
			navLinks: true, // we can click day/week (only if weekNumbers is true) numbers to navigate to specific view 
			//isRTL: true,
			allDaySlot: false,// I canceled this oprion because activities that moving to allDay creating error with the hours in DB
			//businessHours: true, // display business hours // If we want gray background color for sunday and saturday 
			eventLimit: true, // allow "more" link when too many events
			//weekends: false, // If we don't eant to display Saturday and Sunday
			hiddenDays: [6], // hide Saturday
			//fixedWeekCount: true, // False if we want 4.5 - 6 rows of calendar instead of default 6
			weekNumbers: true, // If we want to display week numbers 
			//scrollTime: '06:30:00', // The day start at 6:30 instead of 6:00
			minTime: "07:00:00", // Min Time of every day
			maxTime: "18:00:00", // Max time of every day
			//displayEventTime : false, // If we want to hide the display of time in every event
			nowIndicator: true, // display a marker indicating the current time
			selectHelper: true,
			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
				var start = moment(start).format('YYYY-MM-DD HH:mm:ss'); // new event start time

				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
				var end = moment(end).format('YYYY-MM-DD HH:mm:ss'); // new event end time

				Event = [];
				Event[0] = start;
				Event[1] = end;

				$.ajax({ // send array with start and end times to updateUsersAndStudents.php while edit creating new activity
							type: "POST",
							url: 'createUsersAndStudents.php',
							data: {Event:Event},
							success: function(data)
								{
									$("#createUsersAndStudents").html(data); // insert the values from createUsersAndStudents.php to createUsersAndStudents div
								}
						});

				$('#ModalAdd').modal('show');
			
			},
			eventRender: function(event, element) {  // This function will remember the values while updating -> don't forget change the orange
				element.bind('dblclick', function() {
					$('#ModalEdit #activityId').val(event.activityId);
					$('#ModalEdit #activityName').val(event.activityName);
					$('#ModalEdit #color').val(event.color);
					$('#ModalEdit #start').val(event.start); // PART 1 - very important, data sending to know which date we are working, for multipledelet to not delete history
					$('#ModalEdit #addNumber').val(event.addNumber); //
					$('#ModalEdit #activityTypeId').val(event.activityTypeId);
					$('#ModalEdit #classroomId').val(event.classroomId); // this function store the value right
					$('#ModalEdit #comment').val(event.comment); 
					$('#ModalEdit #addNumber').val(event.addNumber); // this function will set same value to all activity id create in 1 active
					$('#ModalEdit #usersString').val(event.usersString); // this function will set same value to all activity id create in 1 active
		        
						var activityId = event.activityId; // event activityId
						
						var start = event.start.format('YYYY-MM-DD HH:mm:ss'); // event start time

						var end = event.end.format('YYYY-MM-DD HH:mm:ss'); // event end time

// START HERE - this function check if the event is on vecations, if yes so it is hide the input classroom, because it should not show.

						var activityId11 = event.activityTypeId; 
				
				   if (activityId11 == "4" || activityId11 == "2") {
                $("#classA1").hide();
								   $("#colorA1").hide();
            } else {
                  $("#classA1").show();
									 $("#colorA1").show();
            }

						//DONE here

							Event = [];
							Event[0] = start;
							Event[1] = end;
							Event[2] = activityId;

						$.ajax({ // send array with activityId, start and end times to updateUsersAndStudents.php while edit existing activity
							type: "POST",
							url: 'updateUsersAndStudents.php',
							data: {Event:Event},
							success: function(data)
								{
									$("#updateUsersAndStudents").html(data); // insert the values from updateUsersAndStudents.php to updateUsersAndStudents div
								}
						});



					$('#ModalEdit').modal('show');
								 

				});
			element.find('.fc-title').append("<br><u>מורים</u>: " + event.usersString + "<br>");
			// We want to show the team members that belongs to the activity in the activity details
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				swal({
							title: "?האם את/ה בטוח/ה",
							text: "הפעילות החדשה תישמר",
							type: "info", // we can user warning 
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "אשר/י",
							closeOnConfirm: false,
							allowOutsideClick: true,
							cancelButtonText: "ביטול",
							animation: false
						}, 
						// function(){
						// 	edit(event);
						// }
				function (isConfirm) {
					if (isConfirm) {
						edit(event); // If the user confirm the change -> the event will change
					} else {
						revertFunc(); // If the user didn't confirm the change -> the event will revert
					}
      	});
						
			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
				edit(event);
			},
			events: [
			<?php foreach($events as $event):  
				$start = explode(" ", $event['start']);
				$end = explode(" ", $event['end']);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $event['start'];
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $event['end'];
				}
			?>
				{ // This function will remember the values while updating and will set the data that shown 
					activityId: '<?php echo $event['activityId']; ?>',
					activityName: '<?php echo $event['activityName']; ?>',
					title: '<?php echo $event['activityName']; ?>', // I added this for display the activityName
					start: '<?php echo $start; ?>',
					end: '<?php echo $end; ?>',
					color: '<?php echo $event['color']; ?>',
					activityTypeId: '<?php echo $event['activityTypeId']; ?>',
					classroomId: '<?php echo $event['classroomId']; ?>',
					addNumber: '<?php echo $event['addNumber']; ?>',
					usersString: '<?php echo $event['usersString']; ?>',
					comment: '<?php echo $event['comment']; ?>', 
					textColor: 'black', // I added this for black text color instead of white
					borderColor: 'black', // I added this for black border color instead of nothing
					//url: 'www.google.co.il', // If we want url link
					
				},
			<?php endforeach; ?>
			]
		});


			<?php if($authorizationLevel != '1'){ //if the user is not admin-> ALL the fields in ModalAdd and ModalEdit will be disabled (except close button in modal edit)
			?>
				$('#ModalAdd').find('input, textarea, button, select').prop('disabled','disabled');
				$('#ModalEdit').find('input, textarea, #submit, select').prop('disabled','disabled');
				
			<?php } ?>
				<?php if($authorizationLevel != '1' && $authorizationLevel != '2'){ //if the user is not sign in -> ALL the fields in filter be disabled and calendar will destroyed
			?>
				$('#filter').find('input, textarea, button, select').prop('disabled','disabled');
				$('#calendar').fullCalendar('destroy'); // In case we want to destroy calendar after he initialized
			<?php } ?>
		
		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			activityId =  event.activityId;
			
			Event = [];
			Event[0] = activityId;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
			 url: 'editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){
						// alert('האירוע נשמר בהצלחה');
						swal("הפעילות השתנתה בהצלחה", "!כל הכבוד");
					}else{
						// alert('ארעה תקלה בשמירה'); 
						swal("אופס", "!אין חיבור לשרת", "שגיאה");
					}
				}
			});
		}
		// Repeat to the last view that we have been -> Not working so good, We need to ask Adam.
	// var previousDate = moment('<?php echo $start; ?>');
	// $('#calendar').fullCalendar('gotoDate', previousDate);
	});
	

</script>

</body>

</html>