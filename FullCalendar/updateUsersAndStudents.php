<?php
// This file are getting values from index.php and returning to index.php while updating activity
include "bdd.php";
include "db.php"; 
?>
<head>	

	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<!--<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">-->
	<!-- We don't want to use the line above because than it will change the botstrap in every update -->
	<!--<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>--> 
	<!-- We don't want to use the line above because than it will be include twice -->
	<link rel="stylesheet" href="dist/fastselect.min.css">
	<script src="dist/fastselect.standalone.js"></script>

	<style>
			.fstElement { font-size: 1.2em; }
			.fstToggleBtn { min-width: 16.5em; }
			.submitBtn { display: none; }
			.fstMultipleMode { display: block; }
			.fstMultipleMode .fstControls { width: 100%; }
	</style>

</head>

<body>
	
<?php
 if (isset($_POST['Event'][0]) && isset($_POST['Event'][1]) && isset($_POST['Event'][2])){ // $_POST['Event'] came from index.php in the event render function through ajax
	
	$start = $_POST['Event'][0];
	$end = $_POST['Event'][1];
	$activityId = $_POST['Event'][2]; 
  
   ?>
        <label for="users" class="col-sm-2 control-label">אנשי צוות</label>
					<div class="col-sm-10">
					<select class="multipleSelect" name="users_known[]" multiple name="language" id="userNumber">
					<?php
					$result = mysql_query("SELECT `userNumber` FROM `useractivity` WHERE `activityId`= '$activityId' ") or die(mysql_error()); //query for all the users in this activity
							$num_rows = mysql_num_rows($result);
							$users_language = [];
							$i=0;
							while($row = mysql_fetch_assoc($result)) { // Loop to change the array format.
								$users_language[$i] = $row['userNumber']; ?>			 
								<?php
								$i++;
							} 
					$usersFromActivity=$users_language;
					$allAvailableUsers = mysql_query("SELECT `userNumber`, `userName` FROM `user` WHERE (`userNumber` NOT IN (SELECT DISTINCT U.userNumber FROM user U, useractivity UA, activity A WHERE U.userNumber=UA.userNumber AND UA.activityId = A.activityId AND UA.activityId IN (SELECT `activityId` FROM `activity` WHERE start <= '$start' AND end >= '$end'))) OR (`userNumber`IN (SELECT DISTINCT U.userNumber FROM user U, useractivity UA, activity A WHERE U.userNumber=UA.userNumber AND UA.activityId = A.activityId AND UA.activityId = '$activityId'))");
					// $allAvailableUsers = all users that not in other activity at the same time or less. this query will show user even if there is at least 1 minutes that he is free. and will show the users that inside this activity
					$i=0;
					while($userFromList = mysql_fetch_array($allAvailableUsers)) { // loop for mark the users that in this activity
						if(in_array($userFromList["userNumber"],$usersFromActivity)) 
							$str_flag = "selected";
						else $str_flag="";
						?>
						<option value="<?=$userFromList["userNumber"];?>" <?php echo $str_flag; ?>><?=$userFromList["userName"];?></option>
						<!-- 	We want to display userName but to send userNumber  -->
						<?php
						$i++;
					}
					?>
					</select>
						<script>
							$('.multipleSelect').fastselect(); //script to make nice multiple select
						</script>
					</div>
				  </div>

<br>
<br>
<br>

			<label for="users" class="col-sm-2 control-label">תלמידים</label>
					<div class="col-sm-10">
					<select class="multipleSelect" name="students_known[]" multiple name="language">
					<?php
					$result = mysql_query("SELECT `studentId` FROM `studentactivity` WHERE `activityId`= '$activityId' ") or die(mysql_error()); //query for all the students in this activity
							$num_rows = mysql_num_rows($result);
							$students_language = [];
							$i=0;
							while($row = mysql_fetch_assoc($result)) { // Loop to change the array format.
								$students_language[$i] = $row['studentId']; ?>			 
								<?php
								$i++;
							} 
					$studentsFromActivity=$students_language;
					$allAvailableStudents = mysql_query("SELECT `studentId`, `nickName` FROM `student` WHERE (`studentId` NOT IN (SELECT DISTINCT S.studentId FROM student S, studentactivity SA, activity A WHERE S.studentId=SA.studentId AND SA.activityId = A.activityId AND SA.activityId IN (SELECT `activityId` FROM `activity` WHERE start <= '$start' AND end >= '$end'))) OR (`studentId`IN (SELECT DISTINCT S.studentId FROM student S, studentactivity SA, activity A WHERE S.studentId=SA.studentId AND SA.activityId = A.activityId AND SA.activityId = '$activityId')) ORDER BY grade");
					// $allAvailableStudents = all students that not in other activity at the same time or less. this query will show student even if there is at least 1 minutes that he is free. and will show the students that inside this activity
					$i=0;
					while($studentsFromList = mysql_fetch_array($allAvailableStudents)) { // loop for mark the users that in this activity
						if(in_array($studentsFromList["studentId"],$studentsFromActivity)) 
							$str_flag = "selected";
						else $str_flag="";
						?>
						<option value="<?=$studentsFromList["studentId"];?>" <?php echo $str_flag; ?>><?=$studentsFromList["nickName"];?></option>
						<!-- 	We want to display students nickName but to send studentId  -->
						<?php
						$i++;
					}
					?>
					</select>
						<script>
							$('.multipleSelect').fastselect(); //script to make nice multiple select
						</script>
					</div>
				  </div>


				  

 <?php } ?>


 
 </body>