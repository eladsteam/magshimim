<?php

use yii\db\Migration;

class m170307_105023_init_activitychange_table extends Migration
{
    public function up()
    {
				        $this->createTable(
            'activitychange',
            [
                'activityChangeId' => 'pk',
                'activityId' => 'integer',
				'teacherOut' => 'integer',
				'teacherIn' => 'integer',
			
            ],
            'ENGINE=InnoDB'
        );
		
		
		
				     $this->addForeignKey(
            'fk-activitychange-activityId',// This is the fk => the table where i want the fk will be
            'activitychange',// son table
            'activityId', // son pk	
            'activity', // father table
            'activityId', // father pk
            'CASCADE'
        );		
	
		

					 $this->addForeignKey(
            'fk-activitychange-teacherOut',// This is the fk => the table where i want the fk will be
            'activitychange',// son table
            'teacherOut', // son pk	
            'useractivity', // father table
            'userNumber', // father pk
            'CASCADE'
        );
		
		
					$this->addForeignKey(
            'fk-activitychange-teacherIn',// This is the fk => the table where i want the fk will be
            'activitychange',// son table
            'teacherIn', // son pk	
            'user', // father table
            'userNumber', // father pk
            'CASCADE'
        );

    }

	
    public function down()
    {
         $this->dropForeignKey(
				'fk-activitychange-activityTypeId',
				'fk-activitychange-teacherOut',
				'fk-activitychange-teacherIn', 
		 'activitychange');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
