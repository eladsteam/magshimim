<?php

use yii\db\Migration;

class m170112_002939_init_grade_table extends Migration
{
	    public function up()
    {
				        $this->createTable(
            'grade',
            [
                'gradeId' => 'pk',
                'gradeName' => 'string',
			
            ],
            'ENGINE=InnoDB'
        );
	
	
    }

    public function down()
    {
         $this->dropTable('grade');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}