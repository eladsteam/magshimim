<?php

use yii\db\Migration;

class m170121_143225_init_activity_table extends Migration
{
    public function up()
    {
		
			$this->createTable(
            'activity',
            [
                'activityId' => 'pk',
				'activityName' => 'string',
				'start' => 'datetime',
                'end' => 'datetime',
				'color' => 'string',
                'activityTypeId' => 'integer',
                'classroomId' => 'integer',
                'comment' => 'string',
				'addNumber' => 'integer',
				'usersString' => 'string',
				'timeDifferent' => 'integer',
            ],
            'ENGINE=InnoDB'
        );
		
		    $this->addForeignKey(
            'fk-activity-activityTypeId',// This is the fk => the table where i want the fk will be
            'activity',// son table
            'activityTypeId', // son pk	
            'activityType', // father table
            'activityTypeId', // father pk
            'CASCADE'
        );
		
		
		    $this->addForeignKey(
            'fk-activity-classroomId',// This is the fk => the table where i want the fk will be
            'activity',// son table
            'classroomId', // son pk	
            'classroom', // father table
            'classroomId', // father pk
            'CASCADE'
        );
		
		

    }

    public function down()
    {
         $this->dropForeignKey(
		 'fk-activity-activityTypeId',
		 'fk-activity-classroomId',
		 'activity');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}