<?php

use yii\db\Migration;

class m170111_234346_init_specialization_table extends Migration
{
    public function up()
    {
		
			        $this->createTable(
            'specialization',
            [
                'specId' => 'pk',
                'specialization' => 'string',	
            ],
            'ENGINE=InnoDB'
        );
		
		

    }

    public function down()
    {
         $this->dropTable('specialization');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
