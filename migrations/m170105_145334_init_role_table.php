<?php

use yii\db\Migration;

class m170105_145334_init_role_table extends Migration
{
    public function up()
    {
	        $this->createTable(
            'role',
            [
                'roleId' => 'pk',
                'roleType' => 'string'				
            ],
            'ENGINE=InnoDB'
        );
		
	}

    public function down()
	{	
         $this->dropTable('role');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}