<?php

use yii\db\Migration;

class m170112_002344_init_activitytype_table extends Migration
{
	    public function up()
    {
				        $this->createTable(
            'activitytype',
            [
                'activityTypeId' => 'pk',
                'activityTypeName' => 'string',
			
            ],
            'ENGINE=InnoDB'
        );
	
	
    }

    public function down()
    {
         $this->dropTable('activitytype');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
