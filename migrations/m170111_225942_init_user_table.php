<?php

use yii\db\Migration;

class m170111_225942_init_user_table extends Migration
{
    public function up()
    {
	   $this->createTable('user', [            
                'userNumber' => 'pk' ,
                'userId' => 'integer',
				'userName' => 'string',
				'password' => 'string',
				'auth_key'	=> 'string',
				'firstName' => 'string',
				'lastName' => 'string',
				'nickName' => 'string',
				'birth' => 'date',
				'email' => 'string',
				'phone' => 'string',	
				'address' => 'string',	
				'roleName' => 'integer',	
				'startWork' => 'date',
                'verification_code' => 'string',
				'created_at'=>'integer',
				'updated_at'=>'integer',
				'created_by'=>'integer',
				'updated_by'=>'integer'	,
				],
				 'ENGINE=InnoDB'
			);
				 $this->addForeignKey(
            'fk-user-roleName',// This is the fk => the table where i want the fk will be
            'user',// son table
            'roleName', // son pk	
            'role', // father table
            'roleId', // father pk
            'CASCADE'
			);
             
        
    }


	
	
    public function down()
    {
         $this->dropTable(
		 'fk-user-roleName',
		 'user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
