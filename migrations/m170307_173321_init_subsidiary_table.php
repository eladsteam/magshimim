<?php

use yii\db\Migration;

class m170307_173321_init_subsidiary_table extends Migration
{
    public function up()
	{			        $this->createTable(
            'subsidiary',
            [
                'userNumber' => 'pk',
                'teachingHours' => 'integer',	
			    'suspendHours' => 'integer',
			
            ],
            'ENGINE=InnoDB'
        );
	
	
	
		     // add foreign key for table `teacher`
        $this->addForeignKey(
            'fk-subsidiary-userNumber',// This is the fk => the table where i want the fk will be
            'subsidiary',// son table
            'userNumber', // son pk	
            'user', // father table
            'userNumber', // father pk
            'CASCADE'
        );

    }
	
	
    

    public function down()
    {
		
         $this->dropForeignKey(
		 'fk-teacher-userNumber',
		 'subsidiary');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
