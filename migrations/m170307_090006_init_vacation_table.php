<?php

use yii\db\Migration;

class m170307_090006_init_vacation_table extends Migration
{
	    public function up()
    {
				        $this->createTable(
            'vacation',
            [
                'vacationId' => 'pk',
                'userNumber' => 'integer',
				'vacationType' => 'string',
				'startDate' => 'date',
				'endDate' => 'date',
				'duration' => 'integer',
				'commit' => 'string',
			
            ],
            'ENGINE=InnoDB'
        );
		
		     $this->addForeignKey(
            'fk-vacation-userNumber',// This is the fk => the table where i want the fk will be
            'vacation',// son table
            'userNumber', // son pk	
            'user', // father table
            'userNumber', // father pk
            'CASCADE'
        );

    }
	

    public function down()
    {
                $this->dropForeignKey(
		 'fk-vacation-userNumber',
		 'vacation');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
