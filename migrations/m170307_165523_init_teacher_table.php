<?php

use yii\db\Migration;

class m170307_165523_init_teacher_table extends Migration
{
    public function up()
    {
	   $this->createTable(
            'teacher',
            [
                'userNumber' => 'pk',
                'specializationName' => 'integer',
                'teachingHours' => 'integer',	
			    'suspendHours' => 'integer',				
            ],
            'ENGINE=InnoDB'
        );
		

    {
	     // add foreign key for table `teacher`
        $this->addForeignKey(
            'fk-teacher-userNumber',// This is the fk => the table where i want the fk will be
            'teacher',// son table
            'userNumber', // son pk	
            'user', // father table
            'userNumber', // father pk
            'CASCADE'
        );

    }
		
		        $this->addForeignKey(
            'fk-teacher-specializationName',// This is the fk => the table where i want the will be
            'teacher',// son table
            'specializationName', // son pk	
            'specialization', // father table
            'specId', // father pk
            'CASCADE'
        );
		
    }

    public function down()
    {
        // drops foreign key for table `teacher`
        $this->dropForeignKey(
            'fk-teacher-userNumber',
			'fk-teacher-specializationName',
            'teacher'
        );
    }
}
