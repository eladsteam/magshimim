<?php

use yii\db\Migration;

class m170218_175503_init_useractivity_table extends Migration
{
			    public function up()
    {
			 $this->createTable(
            'useractivity',
            [
                'userNumber' => 'integer',
                'activityId' => 'integer',
                'PRIMARY KEY(userNumber, activityId)',		
            ],
            'ENGINE=InnoDB'
        );
		
		
				        $this->addForeignKey(
            'fk-useractivity-userNumber',// This is the fk => the table where i want the fk 
            'useractivity',// son table
            'userNumber', // son pk	
            'user', // father table
            'userNumber', // father pk
            'CASCADE'
        );
		
		
		        $this->addForeignKey(
            'fk-useractivity-activityId',// This is the fk => the table where i want the fk 
            'useractivity',// son table
            'activityId', // son pk	
            'activity', // father table
            'activityId', // father pk
            'CASCADE'
        );
		
		
	}

    public function down()
    {
         $this->dropForeignKey(
		 'fk-useractivity-userNumber',
		 'fk-useractivity-activityId',
		 'useractivity');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

