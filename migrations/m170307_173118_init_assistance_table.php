<?php

use yii\db\Migration;

class m170307_173118_init_assistance_table extends Migration
{
    public function up()
{
				        $this->createTable(
            'assistance',
            [
                'userNumber' => 'pk',
                'teachingHours' => 'integer',	
			    'suspendHours' => 'integer',

			
            ],
            'ENGINE=InnoDB'
        );
		
		
			     // add foreign key for table `assistance`
        $this->addForeignKey(
            'fk-assistance-userNumber',// This is the fk => the table where i want the fk will be
            'assistance',// son table
            'userNumber', // son pk	
            'user', // father table
            'userNumber', // father pk
            'CASCADE'
        );

    }
    

    public function down()
    {
         $this->dropForeignKey(
		 'fk-assistance-userNumber',
		 'assistance');
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
