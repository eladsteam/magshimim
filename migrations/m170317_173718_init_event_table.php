<?php

use yii\db\Migration;

class m170317_173718_init_event_table extends Migration
{
    public function up()
    {
			   $this->createTable(
            'event',
            [
                'idEvent' => 'pk',
                'title' => 'string',
                'description' => 'string',
                'created_date'	=> 'date',				
            ],
            'ENGINE=InnoDB'
        );
	
	
	
    }

    public function down()
    {
        echo "m170308_000825_init_event_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
