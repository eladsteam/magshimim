<?php

use yii\db\Migration;

class m170218_174407_init_studentactivity_table extends Migration
{
    public function up()
	{
			 $this->createTable(
            'studentactivity',
            [
                'studentId' => 'integer',
                'activityId' => 'integer',
                'PRIMARY KEY(studentId, activityId)',
			
            ],
            'ENGINE=InnoDB'
        );
		
		
				        $this->addForeignKey(
            'fk-studentactivity-studentId',// This is the fk => the table where i want the fk 
            'studentactivity',// son table
            'studentId', // son pk	
            'student', // father table
            'studentId', // father pk
            'CASCADE'
        );
		
		
		        $this->addForeignKey(
            'fk-studentactivity-activityId',// This is the fk => the table where i want the fk 
            'studentactivity',// son table
            'activityId', // son pk	
            'activity', // father table
            'activityId', // father pk
            'CASCADE'
        );
		
		
	}

    public function down()
    {
         $this->dropForeignKey(
		 'fk-studentactivity-studentId',
		 'fk-studentactivity-activityId',
		 'studentactivity');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
