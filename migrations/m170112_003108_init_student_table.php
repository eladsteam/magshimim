<?php

use yii\db\Migration;

class m170112_003108_init_student_table extends Migration
{
    public function up()
    {
	   $this->createTable('student', [            
                'studentId' => 'pk' ,
                'studentIdOrigin' => 'integer' ,
                'firstName' => 'string',
				'lastName' => 'string',
				'nickName' => 'string',
				'address'	=> 'string',
				'phone' => 'integer',
				'birth' => 'date',
				'grade' => 'integer',
                'hmo' => 'string',
                'mother_phone' => 'integer',
                'mother_address' => 'string',
                'father_phone' => 'integer',
                'father_address' => 'string',
                'hostel' => 'string',
                'hostel_contect' => 'string',
                'hostel_mail' => 'string',
				
				],
				 'ENGINE=InnoDB'
			);
				 $this->addForeignKey(
            'fk-student-grade',// This is the fk => the table where i want the fk will be
            'student',// son table
            'grade', // son pk	
            'grade', // father table
            'gradeId', // father pk
            'CASCADE'
			);
             
        
    }


	
	
    public function down()
    {
        $this->dropForeignKey(
            'fk-student-grade',
            'student'
        );
		
		 $this->dropTable('grade');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}