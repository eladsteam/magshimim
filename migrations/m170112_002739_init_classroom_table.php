<?php

use yii\db\Migration;

class m170112_002739_init_classroom_table extends Migration
{
	    public function up()
    {
				        $this->createTable(
            'classroom',
            [
                'classroomId' => 'pk',
                'classroomName' => 'string',
			
            ],
            'ENGINE=InnoDB'
        );
	
	
    }

    public function down()
    {
         $this->dropTable('classroom');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}