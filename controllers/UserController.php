<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Teacher;
use app\models\UserSearch;
use app\models\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('indexUser')) // only teachers and principals can watch users 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לצפות ברשימת המשתמשים');

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'userRoles' => Role::getUserRoleWithAllRoles(),    ///////////////////////////// necessary for dropdown 
			'userRole' => $searchModel->roleName,			       ///////////////////////////// necessary for dropdown 			
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principal can view users 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לצפות בפרטי המשתמש');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new User();
    //     $teacher = new Teacher(); //////////////

    //     if ($model->load(Yii::$app->request->post()) && $model->save() && $teacher->load(Yii::$app->request->post()) && $teacher->save() ) { ///////////////
    //         return $this->redirect(['view', 'id' => $model->userNumber]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //             'teacher' => $teacher, //////////////
    //         ]);
    //     }
    // }

    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principal can create new users
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לבצע פעולת יצירת משתמשים חדשים');

        $model = new User();
        //$teacher = new Teacher(); /// New model for create teacher details from user form 

        if ($model->load(Yii::$app->request->post()) //&& $teacher->load(Yii::$app->request->post()) 
        )  { ///////////////
            $model->verification_code = $model->createRandomCode(); // creating random vrtification code - the function is in the user.php
            $model->save();
            // $teacher->userNumber = $model->userNumber; // userNumber is the fk 
            // $teacher->save();
            if($model->roleName == 1){
            return $this->redirect(['teacher/create', 'id' => $model->userNumber]);
            }
                       elseif($model->roleName == 2){
            return $this->redirect(['subsidiary/create', 'id' => $model->userNumber]);
            }
                        elseif($model->roleName == 3){
            return $this->redirect(['assistance/create', 'id' => $model->userNumber]);
            }
        } else {
            $roles = User::getRoles(); // for auth_assignment table
            return $this->render('create', [
                'model' => $model,
                'roles' => $roles, // for auth_assignment table
                //'teacher' => $teacher, // For permission level
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id) /////////// 
    //     {
    //     $model = $this->findModel($id);
    //     if (!$model) {
    //         throw new NotFoundHttpException("The user was not found."); /// maybe
    //     }
    //     $teacher = $this->findModel($id); /// New model for create teacher details from user form
    //     if (!$teacher) {
    //         throw new NotFoundHttpException("The teacher was not found."); /// maybe
    //     }

    //     if ($model->load(Yii::$app->request->post()) && $teacher->load(Yii::$app->request->post()) ) {
    //         $model = User::findOne($id);
    //         $model->save(false);
    //         //$teacher = Teacher::findOne($id);
    //         $teacher->userNumber = $model->userNumber; // userNumber is the fk
    //         $teacher->save(false);
    //         return $this->redirect(['view', 'id' => $model->userNumber]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //             'teacher' => $teacher,
    //         ]); 
    //     }
    // }
        public function actionUpdate($id) 
        {
        
        $model = User::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException("The user was not found.");
        }
        
        //access control
		 if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principals can update users 
		 	throw new UnauthorizedHttpException ('שלום, אינך מורשה לערוך משתמשים');
        
        // In case we eill decide to add rule
        // if (!\Yii::$app->user->can('updateOwnUser') &&  //everyone can update their user profile but only admin can update every user
		//     !\Yii::$app->user->can('ownUserRule', ['user' =>$model]) )
		// 	throw new UnauthorizedHttpException ('Hey, You are not allowed to update users'); 


        // $teacher = Teacher::findOne($model->userNumber); // userNumber is the fk
        // if (!$teacher) {
        //     throw new NotFoundHttpException("The teacher has no profile.");
        // }
        
        //$model->scenario = 'update';
        //$teacher->scenario = 'update';
        
        if ($model->load(Yii::$app->request->post()) //&& $teacher->load(Yii::$app->request->post())
        ) {
            //$isValid = $model->validate();
            //$isValid = $teacher->validate() && $isValid;
            //if ($isValid) {
                $model->save(false);
                //$teacher->save(false);
                return $this->redirect(['user/view', 'id' => $id]);
           // }
        }
        else{
            $roles = User::getRoles();  // For permission level
            return $this->render('update', [
                'model' => $model,
                'roles' => $roles, // for auth_assignment table // i didn't added 1 more row'
                //'teacher' => $teacher,
            ]);
        }
    }

        public function actionChart() ////important for user chart
        { 
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('chart.php', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principals can delete students 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה להסיר משתמשים');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
