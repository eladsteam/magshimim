<?php

namespace app\controllers;

use Yii;
use app\models\Student;
use app\models\StudentSearch;
use app\models\grade;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
         //access control
		if (!\Yii::$app->user->can('indexUser')) // only teachers and principals can watch students 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לצפות ברשימת התלמידים');

        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'studentGrades' => Grade::getStudentGradeWithAllGrades(),    ///////////////////////////// necessary for dropdown 
			'studentGrade' => $searchModel->grade,			       ///////////////////////////// necessary for dropdown
        ]);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //access control
		if (!\Yii::$app->user->can('indexUser')) // every user can view students 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לצפות בפרטי התלמיד');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principal can create new students
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לבצע פעולת יצירת תלמידים חדשים');

        $model = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->studentId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principals can update students 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לערוך תלמידים');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->studentId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only principals can delete students 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה להסיר תלמידים');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('המשתמש לא קיים במערכת');
        }
    }
}
