<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;
use yii\helpers\Url;
use app\models\LostPasswordForm;
use app\models\User;
use app\models\EmailMessage;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        
        $model = new LoginForm();
        $lostPasswordForm = new LostPasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            /* if (Yii::$app->user->identity->userRole == 1){
                 return $this->redirect('http://localhost/a_p/web/events'); 
             }*/
             return $this->goHome();
            //return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model,
            'lostPasswordForm' => $lostPasswordForm,

        ]);
        
    }

    public function actionLogout()
    {
        Yii::$app->homeUrl = ['site/index'];	
		Yii::$app->user->logout();

		return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    //////////////////////////////////////////////////// RBAC  /////////////////////////////////////////
	public function actionRole()
	{
		$auth = Yii::$app->authManager;				
		
		$principal = $auth->createRole('כל ההרשאות');
		$auth->add($principal);
		
		$teacher = $auth->createRole('צפייה בלבד');
		$auth->add($teacher);
		
		// $secretary = $auth->createRole('secretary');
		// $auth->add($secretary);

	}

	public function actionTeacherpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexUser = $auth->createPermission('indexUser');
		$indexUser->description = 'All users can view users';
		$auth->add($indexUser);
		
		$updateOwnUser = $auth->createPermission('updateOwnUser'); 
		$updateOwnUser->description = 'Users can update own user';
		$auth->add($updateOwnUser);	

		$updateOwnPassword = $auth->createPermission('updateOwnPassword'); 
		$updateOwnPassword->description = 'Users can update own password';
		$auth->add($updateOwnPassword);			

	}


	// public function actionSecretarypermissions()
	// {
	// 	$auth = Yii::$app->authManager;

	// 	$updateUser = $auth->createPermission('updateUser'); 
	// 	$updateUser->description = 'Secretary can update users';
	// 	$auth->add($updateUser);	

	// 	$viewUser = $auth->createPermission('viewUser'); 
	// 	$viewUser->description = 'Secretary can view users';
	// 	$auth->add($viewUser);	

	// 	$crudWithoutIndex = $auth->createPermission('crudWithoutIndex'); 
	// 	$crudWithoutIndex->description = 'Secretary can do crud without index ';
	// 	$auth->add($crudWithoutIndex);

	// 	$fullCrudSecretary = $auth->createPermission('fullCrudSecretary'); 
	// 	$fullCrudSecretary->description = 'Secretary can do full crud ';
	// 	$auth->add($fullCrudSecretary);		
	// }
	

	public function actionPrincipalpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$updatePassword = $auth->createPermission('updatePassword'); 
		$updatePassword->description = 'Principal can update all users passwords';
		$auth->add($updatePassword);

		$deleteUser = $auth->createPermission('deleteUser'); 
		$deleteUser->description = 'Principal can delete users';
		$auth->add($deleteUser);

		$createUser = $auth->createPermission('createUser');  
		$createUser->description = 'Principal can create all users';
		$auth->add($createUser);
		
		
		$fullCrudPrincipal = $auth->createPermission('fullCrudPrincipal'); 
		$fullCrudPrincipal->description = 'Principal can do full crud to principals';
		$auth->add($fullCrudPrincipal);
	
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teacher = $auth->getRole('צפייה בלבד'); 

		$indexUser = $auth->getPermission('indexUser'); 
		$auth->addChild($teacher, $indexUser);
		
		$updateOwnUser = $auth->getPermission('updateOwnUser'); 
		$auth->addChild($teacher, $updateOwnUser);

		$updateOwnPassword = $auth->getPermission('updateOwnPassword'); 
		$auth->addChild($teacher, $updateOwnPassword);

		
		////////////////////////////////////
		
		// $secretary = $auth->getRole('secretary'); 
		// $auth->addChild($secretary, $teacher);
		
		// $updateUser = $auth->getPermission('updateUser'); 
		// $auth->addChild($secretary, $updateUser);

		// $viewUser = $auth->getPermission('viewUser'); 
		// $auth->addChild($secretary, $viewUser);	

		// $crudWithoutIndex = $auth->getPermission('crudWithoutIndex'); 
		// $auth->addChild($secretary, $crudWithoutIndex);	

		// $fullCrudSecretary = $auth->getPermission('fullCrudSecretary'); 
		// $auth->addChild($secretary, $fullCrudSecretary);	

		///////////////////////////////////////
		
		$principal = $auth->getRole('כל ההרשאות'); 
		$auth->addChild($principal, $teacher);	
		
		$deleteUser = $auth->getPermission('deleteUser'); 
		$auth->addChild($principal, $deleteUser);
		
		$updatePassword = $auth->getPermission('updatePassword'); 
		$auth->addChild($principal, $updatePassword);
		
		$createUser = $auth->getPermission('createUser'); 
		$auth->addChild($principal, $createUser);
		
		$fullCrudPrincipal = $auth->getPermission('fullCrudPrincipal'); 
		$auth->addChild($principal, $fullCrudPrincipal);
		

	}
		//////////////////////   Rules   ////////////////////////////
		
	// 	public function actionAddfirstrule()
	// {
	// 	$auth = Yii::$app->authManager;
		
	// 	$updateOwnUser = $auth->getPermission('updateOwnUser');
	// 	$auth->remove($updateOwnUser);
		
	// 	$rule = new \app\rbac\OwnUserRule;
	// 	$auth->add($rule);
				
	// 	$updateOwnUser->ruleName = $rule->name;		
	// 	$auth->add($updateOwnUser);	
	// }	
	
	// 	////////////////////////////////////////
	// 	public function actionAddsecondrule()
	// {	
	// 	$auth = Yii::$app->authManager;
		
	// 	$updateOwnPassword = $auth->getPermission('updateOwnPassword');
	// 	$auth->remove($updateOwnPassword);
		
	// 	/////////////////////////////////////////////////עצרנו פה כי לא הצלחנו ליישם 2 הרשאות על אותו החוק למרות שבקורס הצלחנו. צריך לוודא שקיים updateOwnPassword
	// 	$rule = new \app\rbac\OwnUserRule;
	// 	$auth->add($rule);
				
	// 	$updateOwnPassword->ruleName = $rule->name;		
	// 	$auth->add($updateOwnPassword);	
	// }	


	//////////////////////////// forgot password

public function actionRecoverpass() // recoverpass start when the user click on forgot password in the login.php
{
  $model = new FormRecoverPass;
  $msg = null;
  
  if ($model->load(Yii::$app->request->post()))
  {  
    if ($model->validate())
    {
        $users = User::find()->where(['email' => $model->email])->all();
        if($users){ // if the email exist is the systam
        foreach ($users as $user):
            $userId = $user->id; // in case that there is the same email to more then 1 user - the system will recognize only 1 of the users (probably the user witt the bigger userId)
        endforeach;
        
        // This is the mail that will sent to the user
        $subject = "שחזור סיסמה למערכת מגשימים";
        $body = "<p>קוד האימות שלך הינו: ";
        $body .= "<strong>".$user->verification_code."</strong></p>";
        $body .= "<p> נא לא להשיב למייל זה</p>";
        

        //Enviamos el correo
        Yii::$app->mailer->compose()
        ->setTo($model->email)
        ->setFrom('donotereply@magshimim.com')
        ->setSubject($subject)
        ->setHtmlBody($body)
        ->send();
        
        $model->email = null;
        $msg = "נשלחה הודעה אל הדואר האלקטרוני שלך לצורך איפוס סיסמה"; // message that appear after the mail will sent
        $msg .= "<meta http-equiv='refresh' content='1; ".Url::toRoute("site/resetpass")."'>";
        }
        else{ // In case the mail is not exist is the system
            $msg = "הדואר האלקטרוני לא קיים במערכת";
        }
    
    }
        else
        {
            $model->getErrors();
        }
  }
  return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
}
 
 public function actionResetpass() // resetpass start after recoverpass function
 {
  
  $model = new FormResetPass;
  $msg = null;
  
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    
     $newUser = User::findOne(["email" => $model->email, "verification_code" => $model->verification_code]);
     if($newUser){ // if the email exist is the systam
     $newUser->password = $model->password;

     //Si la actualización se lleva a cabo correctamente
     if ($newUser->save())
     {
      
      //Vaciar los campos del formulario
      $model->email = null;
      $model->password = null;
      $model->password_repeat = null;
      $model->verification_code = null;
      
      $msg = "הסיסמה שונתה בהצלחה! הנך מועבר לעמוד הכניסה"; // if all the details are good -> the user will go automaticly to login page
      $msg .= "<meta http-equiv='refresh' content='2; ".Url::toRoute("site/login")."'>";
     }
     else
     {
      $msg = "שגיאה בשליחת המייל";
     }
    }
    else{ // In case the mail is not exist is the system
        $msg = "הדואר האלקטרוני לא קיים במערכת";
    }
   }
  }
  
  return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
  
 }

}
