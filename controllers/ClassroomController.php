<?php

namespace app\controllers;

use Yii;
use app\models\Classroom;
use app\models\ClassroomSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;

/**
 * ClassroomController implements the CRUD actions for Classroom model.
 */
class ClassroomController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Classroom models.
     * @return mixed
     */
    public function actionIndex()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only admin can watch specialization 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לצפות בכיתות הלימוד');

        $searchModel = new ClassroomSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Classroom model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only admin can view specialization 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לצפות בפרטי כיתות הלימוד');

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Classroom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only admin can create specialization 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לבצע פעולת יצירת כיתות לימוד');

        $model = new Classroom();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->classroomId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Classroom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only admin can update specialization 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה לערוך כיתות לימוד');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->classroomId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Classroom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //access control
		if (!\Yii::$app->user->can('fullCrudPrincipal')) // only admin can delete specialization 
			throw new UnauthorizedHttpException ('שלום, אינך מורשה להסיר כיתות לימוד');

        if($id == 3) {
                    // start - not possible delete this class (vecation class) - because then it can make problems with vecations model
            	throw new UnauthorizedHttpException ('שלום, אינך מורשה למחוק כיתת לימוד זו');
                //end here
        }

        elseif($id == 4) {
                    // start - not possible delete this class (vecation class) - because then it can make problems with vecations model
            	throw new UnauthorizedHttpException ('שלום, אינך מורשה למחוק כיתת לימוד זו');
                //end here
        }

        else {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Classroom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Classroom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Classroom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
