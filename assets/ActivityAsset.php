<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ActivityAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [                 // Decleration on the css fules that will affect activity index
        'css/activity.css',
        // 'css/bootstrap.css',  // removed this link to, because the navigation wasnt match to other categorys
        'css/bootstrap-rtl.min.css', // for hebrew
    ];
    public $js = [
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'airani\bootstrap\BootstrapRtlAsset',
    ];
}