

$(function(){
    $(document).on('click','.fc-day',function(){
        var date = $(this).attr('data-date');

        $.get('event/create',{'date':date},function(data){

                $('#modal').modal('show')
                .find('#modalContent')
                .html(data);
        });
    });

$('#modalButton').click(function(){
    //get the click of the create button.

    $('#modal').modal('show')
   .find('#modalContent')
   .html($(this).attr('value'));

});
});